------------------------------------------------------------------------------------------------------------------------
-- Schedule
------------------------------------------------------------------------------------------------------------------------

create table if not exists Timepair (
    id int primary key,
    start_pair time,
    end_pair time
);

create table if not exists Subject (
    id int primary key,
    name varchar
);

create table if not exists Teacher (
    id int primary key,
    first_name varchar,
    middle_name varchar,
    last_name varchar
);

create table if not exists Class (
    id int primary key,
    name varchar
);

create table if not exists Student (
    id int primary key,
    first_name varchar,
    middle_name varchar,
    last_name varchar,
    birthday date,
    address varchar
);

create table if not exists Student_in_class (
    id int primary key,
    class int,
    student int,

    foreign key (class) references Class(id),
    foreign key (student) references Student(id)
);

create table if not exists Schedule (
    id int primary key,
    date date,
    class int,
    number_pair int,
    teacher int,
    subject int,
    classroom int,

    foreign key (class) references Class(id),
    foreign key (number_pair) references Timepair(id),
    foreign key (teacher) references Teacher(id),
    foreign key (subject) references Subject(id)
);

-- insert into Timepair (id, start_pair, end_pair) values (1,	'08:30:00',	'09:15:00');
-- insert into Timepair (id, start_pair, end_pair) values (2,	'09:20:00',	'10:05:00');
-- insert into Timepair (id, start_pair, end_pair) values (3,	'10:15:00',	'11:00:00');
-- insert into Timepair (id, start_pair, end_pair) values (4,	'11:05:00',	'11:50:00');
-- insert into Timepair (id, start_pair, end_pair) values (5,	'12:50:00',	'13:35:00');
-- insert into Timepair (id, start_pair, end_pair) values (6,	'13:40:00',	'14:25:00');
-- insert into Timepair (id, start_pair, end_pair) values (7,	'14:35:00',	'15:20:00');
-- insert into Timepair (id, start_pair, end_pair) values (8,	'15:25:00',	'16:10:00');
--
-- insert into Subject (id, name) values(1, 'Math');
-- insert into Subject (id, name) values(2, 'Russian language');
-- insert into Subject (id, name) values(3, 'Literature');
-- insert into Subject (id, name) values(4, 'Physics');
-- insert into Subject (id, name) values(5, 'Chemistry');
-- insert into Subject (id, name) values(6, 'Geography');
-- insert into Subject (id, name) values(7, 'History');
-- insert into Subject (id, name) values(8, 'Biology');
-- insert into Subject (id, name) values(9, 'English language');
-- insert into Subject (id, name) values(10, 'Informatics');
-- insert into Subject (id, name) values(11, 'Physical Culture');
-- insert into Subject (id, name) values(12, 'Robotics');
-- insert into Subject (id, name) values(13, 'Technology');
--
-- insert into Teacher (id, first_name, middle_name, last_name) values (1,	'Pavel',	'Petrovich',	'Romashkin');
-- insert into Teacher (id, first_name, middle_name, last_name) values (2,	'Vasilij',	'Antonovich',	'Kolesnikov');
-- insert into Teacher (id, first_name, middle_name, last_name) values (3,	'Mariya',	'Stepanovna',	'Vaulina');
-- insert into Teacher (id, first_name, middle_name, last_name) values (4,	'Taisiya',	'Timurovna',	'ZHedrinskaya');
-- insert into Teacher (id, first_name, middle_name, last_name) values (5,	'Marina',	'Gennadevna',	'Sosnovskaya');
-- insert into Teacher (id, first_name, middle_name, last_name) values (6,	'Ekaterina',	'YAkovlevna',	'Myasoedova');
-- insert into Teacher (id, first_name, middle_name, last_name) values (7,	'Liliya',	'Timurovna',	'Arakcheeva');
-- insert into Teacher (id, first_name, middle_name, last_name) values (8,	'Sofya',	'Dmitrievna',	'Krauze');
-- insert into Teacher (id, first_name, middle_name, last_name) values (9,	'Viktoriya',	'Nikolaevna',	'Ostrozhskaya');
-- insert into Teacher (id, first_name, middle_name, last_name) values (10,	'YUrij',	'Georgievich',	'Krylov');
-- insert into Teacher (id, first_name, middle_name, last_name) values (11,	'Andrej',	'Stanislavovich',	'Evseev');
-- insert into Teacher (id, first_name, middle_name, last_name) values (12,	'Bogdan',	'Romanovich',	'Moiseev');
-- insert into Teacher (id, first_name, middle_name, last_name) values (13,	'Aleksandr',	'Vladimirovich',	'Sobolev');
--
-- insert into Class (id, name) values (1,	'8 B');
-- insert into Class (id, name) values (2,	'8 A');
-- insert into Class (id, name) values (3,	'9 C');
-- insert into Class (id, name) values (4,	'9 B');
-- insert into Class (id, name) values (5,	'9 A');
-- insert into Class (id, name) values (6,	'10 B');
-- insert into Class (id, name) values (7,	'10 A');
-- insert into Class (id, name) values (8,	'11 B');
-- insert into Class (id, name) values (9,	'11 A');
--
-- insert into Student (id, first_name, middle_name, last_name, birthday, address) values (1,	'Nikolaj',	'Fedorovich',	'Sokolov',	'2000-10-01T00:00:00.000Z',	'ul. Pushkina, d. 36, kv. 5');
-- insert into Student (id, first_name, middle_name, last_name, birthday, address) values (2,	'Vyacheslav',	'Evgenevich',	'Eliseev',	'2000-11-21T00:00:00.000Z',	'ul. Pushkina, d. 40, kv. 7');
-- insert into Student (id, first_name, middle_name, last_name, birthday, address) values (3,	'Ivan',	'Antonovich',	'Efremov',	'2000-09-19T00:00:00.000Z',	'ul. Pushkina, d. 58, kv. 16');
-- insert into Student (id, first_name, middle_name, last_name, birthday, address) values (4,	'Anatolij',	'Valentinovich',	'ZHdanov',	'2000-07-15T00:00:00.000Z',	'ul. Pushkina, d. 21, kv. 7');
-- insert into Student (id, first_name, middle_name, last_name, birthday, address) values (5,	'Georgij',	'Dmitrievich',	'Noskov',	'2000-03-03T00:00:00.000Z',	'ul. Pushkina, d. 45, kv. 65');
-- insert into Student (id, first_name, middle_name, last_name, birthday, address) values (6,	'Artyom',	'Borisovich',	'Sergeev',	'2000-01-01T00:00:00.000Z',	'ul. Pushkina, d. 1, kv. 5');
-- insert into Student (id, first_name, middle_name, last_name, birthday, address) values (7,	'Arina',	'Fyodorovna',	'Evseeva',	'2000-08-11T00:00:00.000Z',	'ul. Pushkina, d. 21, kv. 51');
-- insert into Student (id, first_name, middle_name, last_name, birthday, address) values (8,	'Angelina',	'Aleksandrovna',	'Voroncova',	'2000-05-21T00:00:00.000Z',	'ul. Pervomajskaya, d. 121, kv. 5');
-- insert into Student (id, first_name, middle_name, last_name, birthday, address) values (9,	'Ekaterina',	'Alekseevna',	'Ustinova',	'2000-01-04T00:00:00.000Z',	'ul. Pervomajskaya, d. 45, kv. 2');
-- insert into Student (id, first_name, middle_name, last_name, birthday, address) values (10,	'Raisa',	'Semyonovna',	'Lapina',	'2000-02-05T00:00:00.000Z',	'ul. Pervomajskaya, d. 56, kv. 7');
-- insert into Student (id, first_name, middle_name, last_name, birthday, address) values (11,	'Leonid',	'Nikitich',	'Ignatov',	'2000-12-30T00:00:00.000Z',	'ul. Pushkina, d. 78, kv. 9');
-- insert into Student (id, first_name, middle_name, last_name, birthday, address) values (12,	'Snezhana',	'YAkovlevna',	'Seliverstova',	'2000-07-23T00:00:00.000Z',	'ul. Pushkina, d. 78, kv. 56');
-- insert into Student (id, first_name, middle_name, last_name, birthday, address) values (13,	'Semyon',	'Vladislavovich',	'Biryukov',	'2000-09-11T00:00:00.000Z',	'ul. CHernova, d. 54, kv. 67');
-- insert into Student (id, first_name, middle_name, last_name, birthday, address) values (14,	'Georgij',	'Vyacheslavovich',	'Baranov',	'2001-01-11T00:00:00.000Z',	'ul. CHernova, d. 56, kv. 89');
-- insert into Student (id, first_name, middle_name, last_name, birthday, address) values (15,	'YUliya',	'Rostislavovna',	'Vishnyakova',	'2001-12-03T00:00:00.000Z',	'ul. Kuratova, d. 96, kv. 45');
-- insert into Student (id, first_name, middle_name, last_name, birthday, address) values (16,	'Valentina',	'Andreevna',	'Bolshakova',	'2001-05-30T00:00:00.000Z',	'ul. Pervomajskaya, d. 89, kv. 67');
-- insert into Student (id, first_name, middle_name, last_name, birthday, address) values (17,	'Leonid',	'Romanovich',	'Kryukov',	'2001-06-04T00:00:00.000Z',	'ul. Zapadnaya, d. 78, kv. 9');
-- insert into Student (id, first_name, middle_name, last_name, birthday, address) values (18,	'Vladislav',	'Sergeevich',	'Cvetkov',	'2001-04-05T00:00:00.000Z',	'ul. Karla Marksa, d. 89, kv. 7');
-- insert into Student (id, first_name, middle_name, last_name, birthday, address) values (19,	'Snezhana',	'Ivanovna',	'Morozova',	'2001-11-06T00:00:00.000Z',	'ul. Internacionalnaya, d. 7, kv. 9');
-- insert into Student (id, first_name, middle_name, last_name, birthday, address) values (20,	'Lyubov',	'Maksimovna',	'Borisova',	'2001-07-12T00:00:00.000Z',	'ul. Kirova, d. 23, kv. 13');
-- insert into Student (id, first_name, middle_name, last_name, birthday, address) values (21,	'Anfisa',	'Antonovna',	'Kalashnikova',	'2001-01-11T00:00:00.000Z',	'ul. Oplesnina, d. 4, kv. 4');
-- insert into Student (id, first_name, middle_name, last_name, birthday, address) values (22,	'Anna',	'Olegovna',	'Osipova',	'2001-04-16T00:00:00.000Z',	'ul. CHernova, d. 32, kv. 56');
-- insert into Student (id, first_name, middle_name, last_name, birthday, address) values (23,	'Kristina',	'Fyodorovna',	'Myasnikova',	'2001-02-03T00:00:00.000Z',	'ul. YUzhnaya, d. 45, kv. 56');
-- insert into Student (id, first_name, middle_name, last_name, birthday, address) values (24,	'Kristina',	'Glebovna',	'Smirnova',	'2001-03-04T00:00:00.000Z',	'ul. Markova, d. 78, kv. 67');
-- insert into Student (id, first_name, middle_name, last_name, birthday, address) values (25,	'Boris',	'Innokentevich',	'Simonov',	'2001-04-05T00:00:00.000Z',	'ul. Kuratova, d. 89, kv. 78');
-- insert into Student (id, first_name, middle_name, last_name, birthday, address) values (26,	'Dmitrij',	'Leonidovich',	'Trofimov',	'2001-05-06T00:00:00.000Z',	'ul. Pushkina, d. 78, kv. 9');
-- insert into Student (id, first_name, middle_name, last_name, birthday, address) values (27,	'YAkov',	'Leonidovich',	'Rozhkov',	'2001-06-07T00:00:00.000Z',	'ul. Karla Marksa, d. 45, kv. 7');
-- insert into Student (id, first_name, middle_name, last_name, birthday, address) values (28,	'Fyodor',	'Rostislavovich',	'Drozdov',	'2001-07-08T00:00:00.000Z',	'ul. Pervomajskaya, d. 23, kv. 34');
-- insert into Student (id, first_name, middle_name, last_name, birthday, address) values (29,	'Gleb',	'Antonovich',	'Strelkov',	'2001-08-09T00:00:00.000Z',	'ul. Zapadnaya, d. 23, kv. 13');
-- insert into Student (id, first_name, middle_name, last_name, birthday, address) values (30,	'Angelina',	'Aleksandrovna',	'Lukina',	'2001-09-10T00:00:00.000Z',	'ul. Internacionalnaya, d. 45, kv. 7');
-- insert into Student (id, first_name, middle_name, last_name, birthday, address) values (31,	'Nina',	'Ilinichna',	'Odincova',	'2001-10-11T00:00:00.000Z',	'ul. Kuratova, d. 4, kv. 76');
-- insert into Student (id, first_name, middle_name, last_name, birthday, address) values (32,	'Valeriya',	'Olegovna',	'Novikova',	'2001-11-12T00:00:00.000Z',	'ul. Kirova, d. 3, kv. 56');
-- insert into Student (id, first_name, middle_name, last_name, birthday, address) values (33,	'Grigorij',	'Gennadevich',	'Kapustin',	'2001-12-13T00:00:00.000Z',	'ul. Pervomajskaya, d. 45, kv. 6');
-- insert into Student (id, first_name, middle_name, last_name, birthday, address) values (34,	'Vitalij',	'Eduardovich',	'Panfilov',	'2001-01-01T00:00:00.000Z',	'ul. CHernova, d. 34, kv. 87');
-- insert into Student (id, first_name, middle_name, last_name, birthday, address) values (35,	'Svyatoslav',	'Vyacheslavovich',	'Tarasov',	'2002-01-14T00:00:00.000Z',	'ul. Pushkina, d. 5, kv. 6');
-- insert into Student (id, first_name, middle_name, last_name, birthday, address) values (36,	'Matvej',	'L',	'YAkushev',	'2002-02-15T00:00:00.000Z',	'ul. Zapadnaya, d. 7, kv. 8');
-- insert into Student (id, first_name, middle_name, last_name, birthday, address) values (37,	'Ilya',	'Stepanovich',	'Alekseev',	'2002-03-16T00:00:00.000Z',	'ul. Pervomajskaya, d. 9, kv. 12');
-- insert into Student (id, first_name, middle_name, last_name, birthday, address) values (38,	'Lyubov',	'Vyacheslavovna',	'Zaharova',	'2002-04-17T00:00:00.000Z',	'ul. CHernova, d. 12, kv. 13');
-- insert into Student (id, first_name, middle_name, last_name, birthday, address) values (39,	'Polina',	'Kirillovna',	'Sidorova',	'2002-05-18T00:00:00.000Z',	'ul. Kuratova, d. 13, kv. 14');
-- insert into Student (id, first_name, middle_name, last_name, birthday, address) values (40,	'Elizaveta',	'Fyodorovna',	'Samojlova',	'2002-06-19T00:00:00.000Z',	'ul. Karla Marksa, d. 46, kv. 45');
-- insert into Student (id, first_name, middle_name, last_name, birthday, address) values (41,	'YUliya',	'Daniilovna',	'Avdeeva',	'2002-07-20T00:00:00.000Z',	'ul. Internacionalnaya, d. 87, kv. 78');
-- insert into Student (id, first_name, middle_name, last_name, birthday, address) values (42,	'Matvej',	'Gennadevich',	'Bogdanov',	'2002-08-21T00:00:00.000Z',	'ul. Pervomajskaya, d. 9, kv. 8');
-- insert into Student (id, first_name, middle_name, last_name, birthday, address) values (43,	'Ilya',	'Egorovich',	'Filippov',	'2002-09-22T00:00:00.000Z',	'ul. CHernova, d. 100, kv. 90');
-- insert into Student (id, first_name, middle_name, last_name, birthday, address) values (44,	'Denis',	'Nikitich',	'Mel',	'2002-10-23T00:00:00.000Z',	'ul. Kirova, d. 123, kv. 45');
-- insert into Student (id, first_name, middle_name, last_name, birthday, address) values (45,	'Svyatoslav',	'Valentinovich',	'Muravyov',	'2002-11-24T00:00:00.000Z',	'ul. Pervomajskaya, d. 45, kv. 67');
-- insert into Student (id, first_name, middle_name, last_name, birthday, address) values (46,	'Anna',	'Denisovna',	'Kulagina',	'2002-12-25T00:00:00.000Z',	'ul. CHernova, d. 7, kv. 78');
-- insert into Student (id, first_name, middle_name, last_name, birthday, address) values (47,	'ZHanna',	'Ilinichna',	'Fokina',	'2002-01-26T00:00:00.000Z',	'ul. Karla Marksa, d. 8, kv. 9');
-- insert into Student (id, first_name, middle_name, last_name, birthday, address) values (48,	'Valeriya',	'YUrevna',	'Lapina',	'2002-02-27T00:00:00.000Z',	'ul. Oplesnina, d. 78, kv. 56');
-- insert into Student (id, first_name, middle_name, last_name, birthday, address) values (49,	'Valentina',	'Andreevna',	'Sazonova',	'2002-03-01T00:00:00.000Z',	'ul. Zapadnaya, d. 84, kv. 48');
-- insert into Student (id, first_name, middle_name, last_name, birthday, address) values (50,	'Nataliya',	'Igorevna',	'Myasnikova',	'2002-04-02T00:00:00.000Z',	'ul. Pushkina, d. 1, kv. 23');
-- insert into Student (id, first_name, middle_name, last_name, birthday, address) values (51,	'Viktoriya',	'YAroslavovna',	'Makarova',	'2002-05-03T00:00:00.000Z',	'ul. Kirova, d. 5, kv. 8');
-- insert into Student (id, first_name, middle_name, last_name, birthday, address) values (52,	'Stanislav',	'Lvovich',	'Lazarev',	'2002-06-04T00:00:00.000Z',	'ul. Internacionalnaya, d. 6, kv. 9');
-- insert into Student (id, first_name, middle_name, last_name, birthday, address) values (53,	'Gennadij',	'Denisovich',	'Ovchinnikov',	'2002-07-05T00:00:00.000Z',	'ul. Kuratova, d. 7, kv. 9');
-- insert into Student (id, first_name, middle_name, last_name, birthday, address) values (54,	'Roman',	'Nikolaevich',	'SHilov',	'2003-08-06T00:00:00.000Z',	'ul. Pushkina, d. 56, kv. 80');
-- insert into Student (id, first_name, middle_name, last_name, birthday, address) values (55,	'Timur',	'Ilich',	'Subbotin',	'2003-09-07T00:00:00.000Z',	'ul. Zapadnaya, d. 47, kv. 39');
-- insert into Student (id, first_name, middle_name, last_name, birthday, address) values (56,	'Danila',	'Ivanovich',	'Osipov',	'2003-10-08T00:00:00.000Z',	'ul. CHernova, d. 78, kv. 69');
-- insert into Student (id, first_name, middle_name, last_name, birthday, address) values (57,	'Arina',	'Timofeevna',	'Silina',	'2003-11-09T00:00:00.000Z',	'ul. CHernova, d. 75, kv. 39');
-- insert into Student (id, first_name, middle_name, last_name, birthday, address) values (58,	'Nadezhda',	'Ilinichna',	'Zaharova',	'2003-12-10T00:00:00.000Z',	'ul. Internacionalnaya, d. 70, kv. 84');
-- insert into Student (id, first_name, middle_name, last_name, birthday, address) values (59,	'Larisa',	'Stanislavovna',	'SHCHerbakova',	'2003-01-11T00:00:00.000Z',	'ul. Pervomajskaya, d. 94, kv. 4');
-- insert into Student (id, first_name, middle_name, last_name, birthday, address) values (60,	'Aleksandra',	'Andreevna',	'Belozyorova',	'2003-02-12T00:00:00.000Z',	'ul. Pushkina, d. 3, kv. 21');
-- insert into Student (id, first_name, middle_name, last_name, birthday, address) values (61,	'Natalya',	'Nikolaevna',	'Davydova',	'2003-03-13T00:00:00.000Z',	'ul. Zapadnaya, d. 4, kv. 2');
-- insert into Student (id, first_name, middle_name, last_name, birthday, address) values (62,	'Mariya',	'Valerievna',	'Fadeeva',	'2003-04-14T00:00:00.000Z',	'ul. CHernova, d. 8, kv. 5');
-- insert into Student (id, first_name, middle_name, last_name, birthday, address) values (63,	'YUrij',	'Denisovich',	'Markov',	'2003-05-15T00:00:00.000Z',	'ul. Kuratova, d. 9, kv. 5');
-- insert into Student (id, first_name, middle_name, last_name, birthday, address) values (64,	'Kirill',	'Vasilevich',	'SHubin',	'2003-06-16T00:00:00.000Z',	'ul. Zapadnaya, d. 6, kv. 3');
-- insert into Student (id, first_name, middle_name, last_name, birthday, address) values (65,	'Grigorij',	'Kirillovich',	'Kolobov',	'2003-07-17T00:00:00.000Z',	'ul. CHernova, d. 9, kv. 34');
-- insert into Student (id, first_name, middle_name, last_name, birthday, address) values (66,	'Semyon',	'Semyonovich',	'Trofimov',	'2003-08-18T00:00:00.000Z',	'ul. Karla Marksa, d. 11, kv. 56');
-- insert into Student (id, first_name, middle_name, last_name, birthday, address) values (67,	'Vasilij',	'Gennadevich',	'Ustinov',	'2003-09-17T00:00:00.000Z',	'ul. Oplesnina, d. 11, kv. 13');
-- insert into Student (id, first_name, middle_name, last_name, birthday, address) values (68,	'Valentina',	'YAkovlevna',	'SHarova',	'2003-10-04T00:00:00.000Z',	'ul. Pervomajskaya, d. 31, kv. 3');
-- insert into Student (id, first_name, middle_name, last_name, birthday, address) values (69,	'Larisa',	'Timurovna',	'Savina',	'2004-11-05T00:00:00.000Z',	'ul. Zapadnaya, d. 22, kv. 33');
-- insert into Student (id, first_name, middle_name, last_name, birthday, address) values (70,	'Galina',	'YAroslavovna',	'Orekhova',	'2004-12-07T00:00:00.000Z',	'ul. Internacionalnaya, d. 77, kv. 87');
-- insert into Student (id, first_name, middle_name, last_name, birthday, address) values (71,	'Arina',	'Ivanovna',	'SHarapova',	'2004-01-08T00:00:00.000Z',	'ul. Kirova, d. 47, kv. 94');
-- insert into Student (id, first_name, middle_name, last_name, birthday, address) values (72,	'Viktoriya',	'YUrevna',	'Sergeeva',	'2004-02-09T00:00:00.000Z',	'ul. Kuratova, d. 44, kv. 87');
-- insert into Student (id, first_name, middle_name, last_name, birthday, address) values (73,	'Vasilij',	'Glebovich',	'Krasilnikov',	'2004-03-15T00:00:00.000Z',	'ul. Zapadnaya, d. 101, kv. 92');
-- insert into Student (id, first_name, middle_name, last_name, birthday, address) values (74,	'Timur',	'Filippovich',	'Rusakov',	'2004-04-26T00:00:00.000Z',	'ul. CHernova, d. 197, kv. 56');
-- insert into Student (id, first_name, middle_name, last_name, birthday, address) values (75,	'Gleb',	'Timofeevich',	'Nesterov',	'2004-05-05T00:00:00.000Z',	'ul. Pervomajskaya, d. 91, kv. 71');
-- insert into Student (id, first_name, middle_name, last_name, birthday, address) values (76,	'Denis',	'Matveevich',	'Makarov',	'2004-05-09T00:00:00.000Z',	'ul. CHernova, d. 41, kv. 31');
-- insert into Student (id, first_name, middle_name, last_name, birthday, address) values (77,	'Elizaveta',	'Borisovna',	'SHilova',	'2004-06-14T00:00:00.000Z',	'ul. Karla Marksa, d. 68, kv. 15');
-- insert into Student (id, first_name, middle_name, last_name, birthday, address) values (78,	'Vera',	'Lvovna',	'Evseeva',	'2004-07-04T00:00:00.000Z',	'ul. Pushkina, d. 5, kv. 13');
-- insert into Student (id, first_name, middle_name, last_name, birthday, address) values (79,	'Margarita',	'Vladislavovna',	'Kabanova',	'2004-08-08T00:00:00.000Z',	'ul. Kuratova, d. 98, kv. 67');
-- insert into Student (id, first_name, middle_name, last_name, birthday, address) values (80,	'Angelina',	'Danilovna',	'Lazareva',	'2004-09-04T00:00:00.000Z',	'ul. Zapadnaya, d. 67, kv. 93');
-- insert into Student (id, first_name, middle_name, last_name, birthday, address) values (81,	'Semyon',	'Anatolevich',	'Voronov',	'2004-10-05T00:00:00.000Z',	'ul. Oplesnina, d. 89, kv. 89');
-- insert into Student (id, first_name, middle_name, last_name, birthday, address) values (82,	'Innokentij',	'Vyacheslavovich',	'Nekrasov',	'2004-01-06T00:00:00.000Z',	'ul. CHernova, d. 777, kv. 77');
-- insert into Student (id, first_name, middle_name, last_name, birthday, address) values (83,	'Artyom',	'Viktorovich',	'Nikitin',	'2004-11-07T00:00:00.000Z',	'ul. Internacionalnaya, d. 666, kv. 7');
-- insert into Student (id, first_name, middle_name, last_name, birthday, address) values (84,	'Egor',	'Petrovich',	'Belyakov',	'2004-12-08T00:00:00.000Z',	'ul. Zapadnaya, d. 87, kv. 9');
--
-- insert into Student_in_class (id, class, student) values (67,	9,	1);
-- insert into Student_in_class (id, class, student) values (68,	9,	2);
-- insert into Student_in_class (id, class, student) values (69,	9,	3);
-- insert into Student_in_class (id, class, student) values (57,	9,	4);
-- insert into Student_in_class (id, class, student) values (71,	9,	5);
-- insert into Student_in_class (id, class, student) values (72,	9,	6);
-- insert into Student_in_class (id, class, student) values (73,	9,	7);
-- insert into Student_in_class (id, class, student) values (74,	9,	8);
-- insert into Student_in_class (id, class, student) values (75,	9,	9);
-- insert into Student_in_class (id, class, student) values (76,	9,	10);
-- insert into Student_in_class (id, class, student) values (77,	9,	11);
-- insert into Student_in_class (id, class, student) values (78,	9,	12);
-- insert into Student_in_class (id, class, student) values (79,	9,	13);
-- insert into Student_in_class (id, class, student) values (80,	8,	14);
-- insert into Student_in_class (id, class, student) values (81,	8,	15);
-- insert into Student_in_class (id, class, student) values (82,	8,	16);
-- insert into Student_in_class (id, class, student) values (83,	8,	17);
-- insert into Student_in_class (id, class, student) values (84,	8,	18);
-- insert into Student_in_class (id, class, student) values (85,	8,	19);
-- insert into Student_in_class (id, class, student) values (86,	8,	20);
-- insert into Student_in_class (id, class, student) values (87,	8,	21);
-- insert into Student_in_class (id, class, student) values (88,	8,	22);
-- insert into Student_in_class (id, class, student) values (89,	8,	23);
-- insert into Student_in_class (id, class, student) values (90,	8,	24);
-- insert into Student_in_class (id, class, student) values (91,	7,	25);
-- insert into Student_in_class (id, class, student) values (92,	7,	26);
-- insert into Student_in_class (id, class, student) values (93,	7,	27);
-- insert into Student_in_class (id, class, student) values (94,	7,	28);
-- insert into Student_in_class (id, class, student) values (95,	7,	29);
-- insert into Student_in_class (id, class, student) values (96,	7,	30);
-- insert into Student_in_class (id, class, student) values (97,	7,	31);
-- insert into Student_in_class (id, class, student) values (98,	7,	32);
-- insert into Student_in_class (id, class, student) values (99,	7,	33);
-- insert into Student_in_class (id, class, student) values (100,	7,	34);
-- insert into Student_in_class (id, class, student) values (101,	6,	35);
-- insert into Student_in_class (id, class, student) values (102,	6,	36);
-- insert into Student_in_class (id, class, student) values (103,	6,	37);
-- insert into Student_in_class (id, class, student) values (104,	6,	38);
-- insert into Student_in_class (id, class, student) values (105,	6,	39);
-- insert into Student_in_class (id, class, student) values (106,	6,	40);
-- insert into Student_in_class (id, class, student) values (107,	6,	41);
-- insert into Student_in_class (id, class, student) values (108,	6,	42);
-- insert into Student_in_class (id, class, student) values (109,	6,	43);
-- insert into Student_in_class (id, class, student) values (110,	6,	44);
-- insert into Student_in_class (id, class, student) values (111,	6,	45);
-- insert into Student_in_class (id, class, student) values (112,	5,	46);
-- insert into Student_in_class (id, class, student) values (113,	5,	47);
-- insert into Student_in_class (id, class, student) values (114,	5,	48);
-- insert into Student_in_class (id, class, student) values (115,	5,	49);
-- insert into Student_in_class (id, class, student) values (116,	5,	50);
-- insert into Student_in_class (id, class, student) values (117,	5,	51);
-- insert into Student_in_class (id, class, student) values (118,	5,	52);
-- insert into Student_in_class (id, class, student) values (119,	5,	53);
-- insert into Student_in_class (id, class, student) values (120,	4,	54);
-- insert into Student_in_class (id, class, student) values (121,	4,	55);
-- insert into Student_in_class (id, class, student) values (122,	4,	56);
-- insert into Student_in_class (id, class, student) values (123,	4,	57);
-- insert into Student_in_class (id, class, student) values (124,	4,	58);
-- insert into Student_in_class (id, class, student) values (125,	4,	59);
-- insert into Student_in_class (id, class, student) values (126,	4,	60);
-- insert into Student_in_class (id, class, student) values (127,	4,	61);
-- insert into Student_in_class (id, class, student) values (128,	4,	62);
-- insert into Student_in_class (id, class, student) values (129,	3,	63);
-- insert into Student_in_class (id, class, student) values (130,	3,	64);
-- insert into Student_in_class (id, class, student) values (131,	3,	65);
-- insert into Student_in_class (id, class, student) values (132,	3,	66);
-- insert into Student_in_class (id, class, student) values (133,	3,	67);
-- insert into Student_in_class (id, class, student) values (134,	3,	68);
-- insert into Student_in_class (id, class, student) values (135,	3,	69);
-- insert into Student_in_class (id, class, student) values (136,	3,	70);
-- insert into Student_in_class (id, class, student) values (137,	2,	71);
-- insert into Student_in_class (id, class, student) values (138,	2,	72);
-- insert into Student_in_class (id, class, student) values (139,	2,	73);
-- insert into Student_in_class (id, class, student) values (140,	2,	74);
-- insert into Student_in_class (id, class, student) values (141,	2,	75);
-- insert into Student_in_class (id, class, student) values (142,	2,	76);
-- insert into Student_in_class (id, class, student) values (143,	2,	77);
-- insert into Student_in_class (id, class, student) values (144,	1,	78);
-- insert into Student_in_class (id, class, student) values (145,	1,	79);
-- insert into Student_in_class (id, class, student) values (146,	1,	80);
-- insert into Student_in_class (id, class, student) values (147,	1,	81);
-- insert into Student_in_class (id, class, student) values (148,	1,	82);
-- insert into Student_in_class (id, class, student) values (149,	1,	83);
-- insert into Student_in_class (id, class, student) values (150,	1,	84);
--
-- insert into Schedule (id, date, class, number_pair, teacher, subject, classroom) values (1,	'2019-09-01T00:00:00.000Z',	9,	1,	11,	1,	47);
-- insert into Schedule (id, date, class, number_pair, teacher, subject, classroom) values (2,	'2019-09-01T00:00:00.000Z',	9,	2,	8,	2,	13);
-- insert into Schedule (id, date, class, number_pair, teacher, subject, classroom) values (3,	'2019-09-01T00:00:00.000Z',	9,	3,	4,	3,	13);
-- insert into Schedule (id, date, class, number_pair, teacher, subject, classroom) values (4,	'2019-09-02T00:00:00.000Z',	9,	1,	4,	3,	13);
-- insert into Schedule (id, date, class, number_pair, teacher, subject, classroom) values (5,	'2019-09-02T00:00:00.000Z',	9,	2,	2,	4,	34);
-- insert into Schedule (id, date, class, number_pair, teacher, subject, classroom) values (6,	'2019-09-02T00:00:00.000Z',	9,	3,	6,	5,	35);
-- insert into Schedule (id, date, class, number_pair, teacher, subject, classroom) values (7,	'2019-09-03T00:00:00.000Z',	9,	1,	5,	6,	36);
-- insert into Schedule (id, date, class, number_pair, teacher, subject, classroom) values (8,	'2019-09-03T00:00:00.000Z',	9,	2,	13,	7,	37);
-- insert into Schedule (id, date, class, number_pair, teacher, subject, classroom) values (9,	'2019-09-03T00:00:00.000Z',	9,	3,	6,	8,	38);
-- insert into Schedule (id, date, class, number_pair, teacher, subject, classroom) values (10,	'2019-09-04T00:00:00.000Z',	9,	1,	9,	9,	39);
-- insert into Schedule (id, date, class, number_pair, teacher, subject, classroom) values (11,	'2019-09-04T00:00:00.000Z',	9,	2,	10,	10,	40);
-- insert into Schedule (id, date, class, number_pair, teacher, subject, classroom) values (12,	'2019-09-04T00:00:00.000Z',	9,	3,	3,	11,	41);
-- insert into Schedule (id, date, class, number_pair, teacher, subject, classroom) values (13,	'2019-09-05T00:00:00.000Z',	9,	1,	3,	13,	43);
-- insert into Schedule (id, date, class, number_pair, teacher, subject, classroom) values (14,	'2019-09-05T00:00:00.000Z',	9,	2,	11,	1,	47);
-- insert into Schedule (id, date, class, number_pair, teacher, subject, classroom) values (15,	'2019-09-05T00:00:00.000Z',	9,	3,	5,	6,	36);
-- insert into Schedule (id, date, class, number_pair, teacher, subject, classroom) values (16,	'2019-08-30T00:00:00.000Z',	9,	1,	2,	4,	34);
-- insert into Schedule (id, date, class, number_pair, teacher, subject, classroom) values (17,	'2019-08-30T00:00:00.000Z',	9,	2,	8,	2,	13);
-- insert into Schedule (id, date, class, number_pair, teacher, subject, classroom) values (18,	'2019-08-30T00:00:00.000Z',	9,	3,	6,	5,	35);
-- insert into Schedule (id, date, class, number_pair, teacher, subject, classroom) values (19,	'2019-08-30T00:00:00.000Z',	9,	4,	10,	1,	47);
-- insert into Schedule (id, date, class, number_pair, teacher, subject, classroom) values (20,	'2019-09-03T00:00:00.000Z',	9,	4,	10,	10,	40);
-- insert into Schedule (id, date, class, number_pair, teacher, subject, classroom) values (21,	'2019-08-30T00:00:00.000Z',	8,	1,	7,	9,	53);
-- insert into Schedule (id, date, class, number_pair, teacher, subject, classroom) values (22,	'2019-08-30T00:00:00.000Z',	8,	2,	7,	9,	53);
-- insert into Schedule (id, date, class, number_pair, teacher, subject, classroom) values (23,	'2019-08-30T00:00:00.000Z',	8,	3,	8,	2,	38);
-- insert into Schedule (id, date, class, number_pair, teacher, subject, classroom) values (24,	'2019-08-30T00:00:00.000Z',	8,	4,	11,	1,	43);
-- insert into Schedule (id, date, class, number_pair, teacher, subject, classroom) values (25,	'2019-08-30T00:00:00.000Z',	8,	5,	8,	3,	39);
-- insert into Schedule (id, date, class, number_pair, teacher, subject, classroom) values (26,	'2019-09-01T00:00:00.000Z',	8,	2,	2,	4,	34);
-- insert into Schedule (id, date, class, number_pair, teacher, subject, classroom) values (27,	'2019-09-01T00:00:00.000Z',	8,	3,	6,	5,	35);
-- insert into Schedule (id, date, class, number_pair, teacher, subject, classroom) values (28,	'2019-09-01T00:00:00.000Z',	8,	4,	12,	6,	36);
-- insert into Schedule (id, date, class, number_pair, teacher, subject, classroom) values (29,	'2019-09-01T00:00:00.000Z',	8,	5,	13,	7,	37);
-- insert into Schedule (id, date, class, number_pair, teacher, subject, classroom) values (30,	'2019-09-02T00:00:00.000Z',	8,	3,	6,	8,	38);
-- insert into Schedule (id, date, class, number_pair, teacher, subject, classroom) values (31,	'2019-09-02T00:00:00.000Z',	8,	4,	7,	9,	53);
-- insert into Schedule (id, date, class, number_pair, teacher, subject, classroom) values (32,	'2019-09-03T00:00:00.000Z',	8,	1,	10,	10,	40);
-- insert into Schedule (id, date, class, number_pair, teacher, subject, classroom) values (33,	'2019-09-03T00:00:00.000Z',	8,	2,	7,	9,	53);
-- insert into Schedule (id, date, class, number_pair, teacher, subject, classroom) values (34,	'2019-09-03T00:00:00.000Z',	8,	3,	7,	9,	53);
-- insert into Schedule (id, date, class, number_pair, teacher, subject, classroom) values (35,	'2019-09-04T00:00:00.000Z',	8,	1,	1,	11,	4);
-- insert into Schedule (id, date, class, number_pair, teacher, subject, classroom) values (36,	'2019-09-04T00:00:00.000Z',	8,	2,	1,	12,	42);
-- insert into Schedule (id, date, class, number_pair, teacher, subject, classroom) values (37,	'2019-09-04T00:00:00.000Z',	8,	3,	3,	13,	43);
-- insert into Schedule (id, date, class, number_pair, teacher, subject, classroom) values (38,	'2019-09-04T00:00:00.000Z',	8,	4,	8,	2,	42);
-- insert into Schedule (id, date, class, number_pair, teacher, subject, classroom) values (39,	'2019-09-04T00:00:00.000Z',	8,	5,	11,	1,	43);
-- insert into Schedule (id, date, class, number_pair, teacher, subject, classroom) values (40,	'2019-09-05T00:00:00.000Z',	8,	2,	11,	1,	43);

-- Сколько всего 10-ых классов
SELECT COUNT(id) as count
FROM Class
WHERE name LIKE '10%';

-- Сколько различных кабинетов школы использовались 2.09.2019 в образовательных целях ?
SELECT COUNT(classroom ) AS count
FROM Schedule
WHERE date = '2019-09-02';

-- Выведите информацию об обучающихся живущих на улице Пушкина (ul. Pushkina)?
SELECT *
FROM Student
WHERE address LIKE 'ul. Pushkina%';

-- Сколько лет самому молодому обучающемуся ?
SELECT MIN(TIMESTAMPDIFF(YEAR, birthday, NOW())) as year
FROM Student;

-- Сколько Анн (Anna) учится в школе ?
SELECT COUNT(id) as count
FROM Student
WHERE first_name = 'Anna';

-- Сколько обучающихся в 10 B классе ?
SELECT COUNT(student) as count
FROM Class
JOIN Student_in_class ON Student_in_class.class = Class.id
WHERE name = '10 B';

-- Выведите название предметов, которые преподает Ромашкин П.П. (Romashkin P.P.) ?
SELECT name as subjects
FROM Subject
JOIN Schedule ON Schedule.subject = Subject.id
JOIN Teacher ON Teacher.id = Schedule.teacher
WHERE
    Teacher.last_name = 'Romashkin' AND
    Teacher.first_name LIKE 'P%' AND
    Teacher.middle_name LIKE 'P%';

-- Во сколько начинается 4-ый учебный предмет по расписанию ?
SELECT start_pair
FROM Timepair
WHERE id = 4;