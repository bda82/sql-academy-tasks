------------------------------------------------------------------------------------------------------------------------
-- Airbnb
------------------------------------------------------------------------------------------------------------------------

create table if not exists Users(
    id int primary key,
    name varchar,
    email varchar,
    email_verified_at date,
    passport varchar,
    phone_number varchar
);

create table if not exists Rooms (
    id int primary key,
    home_type varchar(255),
    address varchar(255),
    has_tv boolean,
    has_internet boolean,
    has_kitchen boolean,
    has_air_con boolean,
    price int,
    owner_id int,
    latitude float4,
    longitude float4,

    foreign key (owner_id) references Users(id)
);

create table if not exists Reservations (
    id int primary key,
    user_id int,
    room_id int,
    start_date date,
    end_date date,
    price int,
    total int,

    foreign key (user_id) references Users(id),
    foreign key (room_id) references Rooms(id)
);

create table if not exists Reviews (
    id int primary key,
    reservation_id int,
    rating int,

    foreign key (reservation_id) references Reservations(id)
);

-- insert into Users (id, name, email, email_verified_at, passport, phone_number) values(1, 'Bruce Willis', 'barjam@hotmail.com', '2016-07-05T12:20:49.000Z', 'o8sdffsmgtswxjvxxl31', '+1 202 555 0128');
-- insert into Users (id, name, email, email_verified_at, passport, phone_number) values(2, 'George Clooney', 'tellis@me.com', '2016-02-22T00:17:42.000Z', 'sx8pav3ggtvfwdz1yte7', '+1 202 555 0145');
-- insert into Users (id, name, email, email_verified_at, passport, phone_number) values(3, 'Kevin Costner', 'metzzo@hotmail.com', '2016-03-26T00:01:52.000Z', 'l51q47urv4hbqqa5f87g', '+1 202 555 0197');
-- insert into Users (id, name, email, email_verified_at, passport, phone_number) values(4, 'Donald Sutherland', 'ralamosm@verizon.net', '2016-11-03T13:01:34.000Z', 'pj2625765oehrlpk6lo4', '+1 202 555 0161');
-- insert into Users (id, name, email, email_verified_at, passport, phone_number) values(5, 'Jennifer Lopez', 'barjam@hotmail.com', '2016-03-02T09:35:57.000Z', 'c70n1nz39ggc9881ybst', '+1 202 555 0145');
-- insert into Users (id, name, email, email_verified_at, passport, phone_number) values(6, 'Ray Liotta', 'jdhedden@comcast.net', '2017-02-24T04:08:45.000Z', 'qpnuvi7mndgl1qmxgdl1',	'+1 202 555 0166');
-- insert into Users (id, name, email, email_verified_at, passport, phone_number) values(7, 'Samuel L. Jackson', 'moonlapse@outlook.com',	'2018-07-19T11:16:13.000Z', 'i6yvht95527z3idgqx9y',	'+1 202 555 0162');
-- insert into Users (id, name, email, email_verified_at, passport, phone_number) values(8, 'Nikole Kidman', 'okroeger@msn.com', '2016-09-17T14:47:01.000Z', '3bhg1l720u1reugsmbfc', '+1 202 555 0146');
-- insert into Users (id, name, email, email_verified_at, passport, phone_number) values(9, 'Alan Rickman', 'karasik@verizon.net', '2016-10-03T14:47:09.000Z', '86ezqmymrhof0w81x3n9',	'+1 202 555 0182');
-- insert into Users (id, name, email, email_verified_at, passport, phone_number) values(10, 'Kurt Russell', 'gator@live.com', '2016-08-18T22:26:00.000Z',	'4wwzx2kpn81410q79ieh',	'+1 202 555 0134');
-- insert into Users (id, name, email, email_verified_at, passport, phone_number) values(11, 'Harrison Ford', 'kostas@hotmail.com', '2016-12-05T07:35:14.000Z', 'u8sxb8byhaajee65n9t2', '+1 202 555 0124');
-- insert into Users (id, name, email, email_verified_at, passport, phone_number) values(12, 'Russell Crowe', 'glenz@att.net', '2018-01-04T21:20:27.000Z',	'ywtkc8tns9le6aioalyf',	'+1 202 555 0171');
-- insert into Users (id, name, email, email_verified_at, passport, phone_number) values(13, 'Steve Martin', 'nelson@outlook.com', '2016-07-29T04:25:00.000Z',	'w76yphg3kvzg77ilmxfs',	'+1 202 555 0138');
-- insert into Users (id, name, email, email_verified_at, passport, phone_number) values(14, 'Michael Caine', 'dmouse@live.com', '2016-04-24T01:31:46.000Z', '7k6m698ky9de34yc9zrh',	'+1 202 555 0151');
-- insert into Users (id, name, email, email_verified_at, passport, phone_number) values(15, 'Angelina Jolie', 'chance@att.net', '2018-06-22T14:07:50.000Z', 'f403jj1cpkzoiwicn0kz',	'+1 202 555 0173');
-- insert into Users (id, name, email, email_verified_at, passport, phone_number) values(16, 'Mel Gibson', 'rohitm@yahoo.ca', '2016-01-02T11:42:58.000Z',	'hojjtsy7rt0v7we42vlr',	'+1 202 555 0152');
-- insert into Users (id, name, email, email_verified_at, passport, phone_number) values(17, 'Michael Douglas', 'timtroyr@hotmail.com', '2016-08-02T12:50:51.000Z', '03ga9r324u8n25ujb5ue', '+1 202 555 0106');
-- insert into Users (id, name, email, email_verified_at, passport, phone_number) values(18, 'John Travolta', 'wainwrig@msn.com', '2016-11-19T12:30:43.000Z',	'fzjhl0v82o0amalr8649',	'+1 202 555 0176');
-- insert into Users (id, name, email, email_verified_at, passport, phone_number) values(19, 'Sylvester Stallone', 'kohlis@aol.com', '2018-10-04T00:16:57.000Z',	'b26226km8w1qfzqpi9so',	'+353 20 910 6141');
-- insert into Users (id, name, email, email_verified_at, passport, phone_number) values(20, 'Tommy Lee Jones', 'szymansk@live.com', '2018-03-09T11:08:34.000Z', 'c46s9eodm9k3qiwvju3y',	'+353 20 912 2322');
-- insert into Users (id, name, email, email_verified_at, passport, phone_number) values(21, 'Catherine Zeta Jones',	'flakeg@hotmail.com', '2016-04-02T12:28:19.000Z', 'izq356fwuzdhc6u1skan', '+353 20 911 3955');
-- insert into Users (id, name, email, email_verified_at, passport, phone_number) values(22, 'Antonio Banderas', 'sassen@verizon.net', '2018-03-23T08:15:47.000Z',	'wftjnqkjfqi7dj7g6oeo',  '+353 20 917 9717');
-- insert into Users (id, name, email, email_verified_at, passport, phone_number) values(23, 'Kim Basinger', 'jaesenj@sbcglobal.net', '2017-09-27T00:28:22.000Z', 'h60m7wovjopzp9kx4m3m', '+353 20 913 3636');
-- insert into Users (id, name, email, email_verified_at, passport, phone_number) values(24, 'Sam Neill', 'clkao@live.com', '2017-02-05T22:24:13.000Z', '3rpgiccs25fj7apg246s', '+353 20 914 1612');
-- insert into Users (id, name, email, email_verified_at, passport, phone_number) values(25, 'Hideo Kojima', 'admin@sql-academy.org',	'2017-03-08T14:15:51.000Z', '97shpolky5vg3b2qn2wu',	'+7 401 452 0052');
-- insert into Users (id, name, email, email_verified_at, passport, phone_number) values(26, 'ClINT Eastwood', 'froodian@verizon.net', '2019-02-10T09:43:59.000Z',	'ogbjpnp7vlzwxu4bj0bs', '+7 401 722 0912');
-- insert into Users (id, name, email, email_verified_at, passport, phone_number) values(27, 'Brad Pitt', 'kewley@optonline.net', '2017-02-11T05:45:15.000Z', '829j2ygocn8btzae49kv', '+7 401 741 3797');
-- insert into Users (id, name, email, email_verified_at, passport, phone_number) values(28, 'Johnny Depp', 'cgarcia@yahoo.ca', '2017-05-26T01:19:06.000Z', 'qpp6hbnae42cdhmxlk4j', '+7 401 195 7363');
-- insert into Users (id, name, email, email_verified_at, passport, phone_number) values(29, 'Pierce Brosnan', 'treeves@icloud.com', '2019-03-08T01:56:00.000Z', 'lqiwecclne9rv8woo2go', '+7 401 749 3620');
-- insert into Users (id, name, email, email_verified_at, passport, phone_number) values(30, 'Sean Connery', 'jschauma@icloud.com', '2016-05-21T00:45:17.000Z', 'lyh4jkdxkvtvulvqi5db', '+7 401 511 6783');
-- insert into Users (id, name, email, email_verified_at, passport, phone_number) values(31, 'Bruce Willis', 'kewley@icloud.com', '2016-12-08T20:18:59.000Z', '0ofa2khvnptiackbssv0', '+375 154 771 3462');
-- insert into Users (id, name, email, email_verified_at, passport, phone_number) values(32, 'Mullah Omar', 'jgoerzen@me.com', '2019-07-18T06:50:42.000Z', 'f3ft4fwo7vafvqb0ugk5', '+375 154 802 9505');
-- insert into Users (id, name, email, email_verified_at, passport, phone_number) values(33, 'Vasanta Roberta', 'roberta@me.com', '2017-08-11T06:43:43.000Z', 'f3ftsdfsdffvqb0ugk5', '+11 444 812 1315');
--
-- insert into Rooms (id, home_type, address, has_tv, has_internet, has_kitchen, has_air_con, price, owner_id, latitude, longitude) values (1, 'Private room', '11218, Friel Place, New York', true, true, true, false, 149, 1, 40.6475, -73.9724);
-- insert into Rooms (id, home_type, address, has_tv, has_internet, has_kitchen, has_air_con, price, owner_id, latitude, longitude) values (2, 'Entire home/apt', '10018, 6th Avenue, New York', false, true, true, true, 225, 1, 40.7536, -73.9838);
-- insert into Rooms (id, home_type, address, has_tv, has_internet, has_kitchen, has_air_con, price, owner_id, latitude, longitude) values (3, 'Private room', '10027, West 128th Street, New York', true, false, false, true, 150, 2, 40.809, -73.9419);
-- insert into Rooms (id, home_type, address, has_tv, has_internet, has_kitchen, has_air_con, price, owner_id, latitude, longitude) values (4, 'Entire home/apt', '11238, Gates Avenue, New York', true, false, false, true, 89, 2, 40.6851, -73.9598);
-- insert into Rooms (id, home_type, address, has_tv, has_internet, has_kitchen, has_air_con, price, owner_id, latitude, longitude) values (5, 'Entire home/apt', '10029, Park Avenue, New York', false, true, true, true, 80, 3, 40.7985, -73.944);
-- insert into Rooms (id, home_type, address, has_tv, has_internet, has_kitchen, has_air_con, price, owner_id, latitude, longitude) values (6, 'Entire home/apt', '10016, East 38th Street, New York', false, true, false, false, 200, 3, 40.7477, -73.975);
-- insert into Rooms (id, home_type, address, has_tv, has_internet, has_kitchen, has_air_con, price, owner_id, latitude, longitude) values (7, 'Private room', '11216, Lexington Avenue, New York', false, false, true, true, 60, 4, 40.6869, -73.956);
-- insert into Rooms (id, home_type, address, has_tv, has_internet, has_kitchen, has_air_con, price, owner_id, latitude, longitude) values (8, 'Private room', '10019, West 53rd Street, New York', true, false, false, true, 79, 14, 40.7649, -73.9849);
-- insert into Rooms (id, home_type, address, has_tv, has_internet, has_kitchen, has_air_con, price, owner_id, latitude, longitude) values (9, 'Private room', '10025, West 107th Street, New York', true, false, true, false, 79, 6, 40.8018, -73.9672);
-- insert into Rooms (id, home_type, address, has_tv, has_internet, has_kitchen, has_air_con, price, owner_id, latitude, longitude) values (10, 'Entire home/apt', '10002, Rutgers Street, New York', true, false, true, false, 150, 7, 40.7134, -73.9904);
-- insert into Rooms (id, home_type, address, has_tv, has_internet, has_kitchen, has_air_con, price, owner_id, latitude, longitude) values (11, 'Entire home/apt', '10025, West 109th Street, New York', true, true, false, false, 135, 8, 40.8032, -73.9654);
-- insert into Rooms (id, home_type, address, has_tv, has_internet, has_kitchen, has_air_con, price, owner_id, latitude, longitude) values (12, 'Private room', '10036, West 47th Street, New York', false, false, true, true, 85, 10, 40.7608, -73.9887);
-- insert into Rooms (id, home_type, address, has_tv, has_internet, has_kitchen, has_air_con, price, owner_id, latitude, longitude) values (13, 'Private room', '11215, 11th Street, New York', false, false, false, false, 89, 11, 40.6683, -73.9878);
-- insert into Rooms (id, home_type, address, has_tv, has_internet, has_kitchen, has_air_con, price, owner_id, latitude, longitude) values (14, 'Private room', '10025, West 106th Street, New York', false, false, false, true, 85, 12, 40.7983, -73.9611);
-- insert into Rooms (id, home_type, address, has_tv, has_internet, has_kitchen, has_air_con, price, owner_id, latitude, longitude) values (15, 'Entire home/apt', '10014, Perry Street, New York', true, false, false, true, 120, 13, 40.7353, -74.0052);
-- insert into Rooms (id, home_type, address, has_tv, has_internet, has_kitchen, has_air_con, price, owner_id, latitude, longitude) values (16, 'Entire home/apt', '11211, South 4th Street, New York', false, false, true, false, 140, 14, 40.7084, -73.9535);
-- insert into Rooms (id, home_type, address, has_tv, has_internet, has_kitchen, has_air_con, price, owner_id, latitude, longitude) values (17, 'Entire home/apt', '11205, Willoughby Avenue, New York', false, false, true, false, 215, 15, 40.6917, -73.9718);
-- insert into Rooms (id, home_type, address, has_tv, has_internet, has_kitchen, has_air_con, price, owner_id, latitude, longitude) values (18, 'Private room', '10011, West 21st Street, New York', true, true, false, true, 140, 16, 40.7419, -73.995);
-- insert into Rooms (id, home_type, address, has_tv, has_internet, has_kitchen, has_air_con, price, owner_id, latitude, longitude) values (19, 'Entire home/apt', '11216, Bergen Street, New York', true, true, true, true, 99, 17, 40.6759, -73.9469);
-- insert into Rooms (id, home_type, address, has_tv, has_internet, has_kitchen, has_air_con, price, owner_id, latitude, longitude) values (20, 'Entire home/apt', '10026-2864, Duke Ellington Circle, New York', false, false, false,	false, 190,	18, 40.7968, -73.9487);
-- insert into Rooms (id, home_type, address, has_tv, has_internet, has_kitchen, has_air_con, price, owner_id, latitude, longitude) values (21, 'Entire home/apt', '11249, North 8th Street, New York', true, true, true, true, 299, 19, 40.7184, -73.9572);
-- insert into Rooms (id, home_type, address, has_tv, has_internet, has_kitchen, has_air_con, price, owner_id, latitude, longitude) values (22, 'Private room', '11217, Saint Marks Avenue, New York', true, false, false, true, 130, 20, 40.6807, -73.9771);
-- insert into Rooms (id, home_type, address, has_tv, has_internet, has_kitchen, has_air_con, price, owner_id, latitude, longitude) values (23, 'Private room', '11217, 5th Avenue, New York',	true, true, true, true, 80,	21,	40.6799, -73.978);
-- insert into Rooms (id, home_type, address, has_tv, has_internet, has_kitchen, has_air_con, price, owner_id, latitude, longitude) values (24, 'Private room', '11217, 5th Avenue, New York',	true, true, true, false, 110, 22, 40.68,	-73.9787);
-- insert into Rooms (id, home_type, address, has_tv, has_internet, has_kitchen, has_air_con, price, owner_id, latitude, longitude) values (25, 'Entire home/apt', '11221, Hancock Street, New York', true, false, false, false, 120, 23, 40.6837, -73.9403);
-- insert into Rooms (id, home_type, address, has_tv, has_internet, has_kitchen, has_air_con, price, owner_id, latitude, longitude) values (26, 'Private room', '11218, Terrace Place, New York', true, true, false, false, 60, 24, 40.656, -73.9752);
-- insert into Rooms (id, home_type, address, has_tv, has_internet, has_kitchen, has_air_con, price, owner_id, latitude, longitude) values (27, 'Private room', '10034, Seaman Avenue, New York', true, true, false, false, 80, 25, 40.8675, -73.9264);
-- insert into Rooms (id, home_type, address, has_tv, has_internet, has_kitchen, has_air_con, price, owner_id, latitude, longitude) values (28, 'Entire home/apt', '10019, West 56th Street, New York', true, true, true, false, 150, 26, 40.7672, -73.9853);
-- insert into Rooms (id, home_type, address, has_tv, has_internet, has_kitchen, has_air_con, price, owner_id, latitude, longitude) values (29, 'Private room', '10034, Post Avenue, New York', true, false, false, false, 44, 27, 40.8648, -73.9211);
-- insert into Rooms (id, home_type, address, has_tv, has_internet, has_kitchen, has_air_con, price, owner_id, latitude, longitude) values (30, 'Entire home/apt', '10003, East 10th Street, New York', false, true, true, false, 180, 28, 40.7292, -73.9854);
-- insert into Rooms (id, home_type, address, has_tv, has_internet, has_kitchen, has_air_con, price, owner_id, latitude, longitude) values (31, 'Private room', '10031, West 140th Street, New York', false, false, true, true, 50, 29, 40.8224, -73.951);
-- insert into Rooms (id, home_type, address, has_tv, has_internet, has_kitchen, has_air_con, price, owner_id, latitude, longitude) values (32, 'Private room', '10027, West 126th Street, New York', false, false, true, true, 52, 30, 40.813, -73.9547);
-- insert into Rooms (id, home_type, address, has_tv, has_internet, has_kitchen, has_air_con, price, owner_id, latitude, longitude) values (33, 'Private room', '11222, Lombardy Street, New York', false, true, false, false, 55, 31, 40.7222, -73.9376);
-- insert into Rooms (id, home_type, address, has_tv, has_internet, has_kitchen, has_air_con, price, owner_id, latitude, longitude) values (34, 'Private room', '10031, West 138th Street, New York', true, false, true, false, 50, 32, 40.8213, -73.9532);
-- insert into Rooms (id, home_type, address, has_tv, has_internet, has_kitchen, has_air_con, price, owner_id, latitude, longitude) values (35, 'Private room', '11216, Jefferson Avenue, New York', true, false, false, false, 70, 32, 40.6831, -73.9547);
-- insert into Rooms (id, home_type, address, has_tv, has_internet, has_kitchen, has_air_con, price, owner_id, latitude, longitude) values (36, 'Private room', '11215, 10th Street, New York', true, false, false, true, 89, 32, 40.6687, -73.9878);
-- insert into Rooms (id, home_type, address, has_tv, has_internet, has_kitchen, has_air_con, price, owner_id, latitude, longitude) values (37, 'Private room', '11221, Lexington Avenue, New York', false, true, false, false, 35, 2, 40.6888, -73.9431);
-- insert into Rooms (id, home_type, address, has_tv, has_internet, has_kitchen, has_air_con, price, owner_id, latitude, longitude) values (38, 'Entire home/apt', '11237, Troutman Street, New York', true, false, true, false, 85, 1, 40.7019, -73.9275);
-- insert into Rooms (id, home_type, address, has_tv, has_internet, has_kitchen, has_air_con, price, owner_id, latitude, longitude) values (39, 'Private room', '11226, Marlborough Road, New York', false, true, false, false, 150, 25, 40.637, -73.9633);
-- insert into Rooms (id, home_type, address, has_tv, has_internet, has_kitchen, has_air_con, price, owner_id, latitude, longitude) values (40, 'Shared room', '10002, East Broadway, New York', true, true, true, true, 40, 25, 40.714, -73.9892);
-- insert into Rooms (id, home_type, address, has_tv, has_internet, has_kitchen, has_air_con, price, owner_id, latitude, longitude) values (41, 'Private room', '10009, East 4th Street, New York', false, false, false, false, 68, 25, 40.7229, -73.982);
-- insert into Rooms (id, home_type, address, has_tv, has_internet, has_kitchen, has_air_con, price, owner_id, latitude, longitude) values (42, 'Entire home/apt', '11215, 13th Street, New York',	true, true, true, false, 120, 25, 40.6628, -73.9797);
-- insert into Rooms (id, home_type, address, has_tv, has_internet, has_kitchen, has_air_con, price, owner_id, latitude, longitude) values (43, 'Private room', '11205, Park Avenue, New York', true, false, true, true, 120, 12, 40.6967, -73.9758);
-- insert into Rooms (id, home_type, address, has_tv, has_internet, has_kitchen, has_air_con, price, owner_id, latitude, longitude) values (44, 'Private room', '10024, West 87th Street, New York', false, true, true, false, 135, 21, 40.7901, -73.9793);
-- insert into Rooms (id, home_type, address, has_tv, has_internet, has_kitchen, has_air_con, price, owner_id, latitude, longitude) values (45, 'Entire home/apt', '10027, West 130th Street, New York', false, true, true, false, 150, 21, 40.8117, -73.9448);
-- insert into Rooms (id, home_type, address, has_tv, has_internet, has_kitchen, has_air_con, price, owner_id, latitude, longitude) values (46, 'Entire home/apt', '11225, Ocean Avenue, New York', false, false, true, true, 150, 21, 40.6594, -73.9624);
-- insert into Rooms (id, home_type, address, has_tv, has_internet, has_kitchen, has_air_con, price, owner_id, latitude, longitude) values (47, 'Private room', '11101, 21st Street, New York', true, false, false, true, 130, 26, 40.7477, -73.9474);
-- insert into Rooms (id, home_type, address, has_tv, has_internet, has_kitchen, has_air_con, price, owner_id, latitude, longitude) values (48, 'Entire home/apt', '11207, Fulton Street, New York', true, true, false, false, 110, 16, 40.6811, -73.9559);
-- insert into Rooms (id, home_type, address, has_tv, has_internet, has_kitchen, has_air_con, price, owner_id, latitude, longitude) values (49, 'Entire home/apt', '11221, Throop Avenue, New York', true, true, false, false, 115, 15, 40.6855, -73.9409);
-- insert into Rooms (id, home_type, address, has_tv, has_internet, has_kitchen, has_air_con, price, owner_id, latitude, longitude) values (50, 'Private room', '11205, Washington Park, New York', true, false, true, true, 80, 15, 40.6914, -73.9738);
--
-- insert into Reservations (id, user_id, room_id, start_date, end_date, price, total) values (1, 1, 1, '2018-11-13T12:00:00.000Z', '2018-11-15T12:00:00.000Z', 140, 280);
-- insert into Reservations (id, user_id, room_id, start_date, end_date, price, total) values (2, 2, 1, '2018-10-09T12:00:00.000Z', '2018-10-12T12:00:00.000Z', 120, 360);
-- insert into Reservations (id, user_id, room_id, start_date, end_date, price, total) values (3, 12, 1, '2018-12-29T12:00:00.000Z', '2019-01-07T12:00:00.000Z', 180, 1620);
-- insert into Reservations (id, user_id, room_id, start_date, end_date, price, total) values (4, 13, 1, '2019-02-01T12:00:00.000Z', '2019-02-02T12:00:00.000Z', 145, 145);
-- insert into Reservations (id, user_id, room_id, start_date, end_date, price, total) values (5, 17, 1, '2019-02-02T12:00:00.000Z', '2019-02-04T12:00:00.000Z', 149, 298);
-- insert into Reservations (id, user_id, room_id, start_date, end_date, price, total) values (6, 14, 2, '2020-03-21T09:00:00.000Z', '2020-03-23T09:00:00.000Z', 225, 450);
-- insert into Reservations (id, user_id, room_id, start_date, end_date, price, total) values (7, 19, 13, '2020-03-21T10:00:00.000Z', '2020-04-21T10:00:00.000Z', 89, 2679);
-- insert into Reservations (id, user_id, room_id, start_date, end_date, price, total) values (8, 29, 16, '2019-06-14T10:00:00.000Z', '2019-06-24T10:00:00.000Z', 140, 1400);
-- insert into Reservations (id, user_id, room_id, start_date, end_date, price, total) values (9, 32, 21, '2019-05-11T10:00:00.000Z', '2019-05-14T10:00:00.000Z', 270, 810);
-- insert into Reservations (id, user_id, room_id, start_date, end_date, price, total) values (10, 13, 21, '2019-11-04T10:00:00.000Z', '2019-11-06T10:00:00.000Z', 280, 560);
-- insert into Reservations (id, user_id, room_id, start_date, end_date, price, total) values (11, 9, 21, '2020-02-28T10:00:00.000Z', '2020-02-29T10:00:00.000Z', 290, 290);
-- insert into Reservations (id, user_id, room_id, start_date, end_date, price, total) values (12, 7, 19, '2020-05-01T10:00:00.000Z', '2020-05-02T10:00:00.000Z', 99, 99);
-- insert into Reservations (id, user_id, room_id, start_date, end_date, price, total) values (13, 15, 8, '2020-01-19T12:00:00.000Z', '2020-01-21T12:00:00.000Z', 78, 156);
-- insert into Reservations (id, user_id, room_id, start_date, end_date, price, total) values (14, 13, 7, '2019-09-13T10:00:00.000Z', '2019-09-17T10:00:00.000Z', 60, 240);
-- insert into Reservations (id, user_id, room_id, start_date, end_date, price, total) values (15, 12, 5, '2020-05-14T10:00:00.000Z', '2020-05-15T10:00:00.000Z', 80, 80);
-- insert into Reservations (id, user_id, room_id, start_date, end_date, price, total) values (16, 10, 5, '2020-03-26T10:00:00.000Z', '2020-03-27T10:00:00.000Z', 80, 80);
-- insert into Reservations (id, user_id, room_id, start_date, end_date, price, total) values (17, 11, 50, '2019-11-18T11:00:00.000Z', '2019-11-25T11:00:00.000Z', 80, 560);
-- insert into Reservations (id, user_id, room_id, start_date, end_date, price, total) values (18, 27, 49, '2019-09-18T11:00:00.000Z', '2019-09-20T11:00:00.000Z', 110, 220);
-- insert into Reservations (id, user_id, room_id, start_date, end_date, price, total) values (19, 28, 49, '2020-04-01T11:00:00.000Z', '2020-04-02T11:00:00.000Z', 115, 115);
-- insert into Reservations (id, user_id, room_id, start_date, end_date, price, total) values (20, 1, 49, '2020-06-01T10:00:00.000Z', '2020-06-11T10:00:00.000Z', 115, 1150);
-- insert into Reservations (id, user_id, room_id, start_date, end_date, price, total) values (21, 2, 48, '2019-11-07T10:00:00.000Z', '2019-11-10T10:00:00.000Z', 110, 330);
-- insert into Reservations (id, user_id, room_id, start_date, end_date, price, total) values (22, 12, 32, '2020-01-14T13:00:00.000Z', '2020-01-18T13:00:00.000Z', 52, 208);
-- insert into Reservations (id, user_id, room_id, start_date, end_date, price, total) values (23, 12, 32, '2019-09-17T13:00:00.000Z', '2019-09-18T13:00:00.000Z', 52, 52);
-- insert into Reservations (id, user_id, room_id, start_date, end_date, price, total) values (24, 7, 19, '2020-01-08T13:00:00.000Z', '2020-01-11T13:00:00.000Z', 99, 297);
-- insert into Reservations (id, user_id, room_id, start_date, end_date, price, total) values (25, 18, 19, '2019-11-01T10:00:00.000Z', '2019-11-16T10:00:00.000Z', 99, 1485);
-- insert into Reservations (id, user_id, room_id, start_date, end_date, price, total) values (26, 21, 17, '2019-11-03T09:00:00.000Z', '2019-11-05T09:00:00.000Z', 215, 430);
-- insert into Reservations (id, user_id, room_id, start_date, end_date, price, total) values (27, 31, 25, '2020-04-20T09:00:00.000Z', '2020-04-22T09:00:00.000Z', 120, 240);
-- insert into Reservations (id, user_id, room_id, start_date, end_date, price, total) values (28, 21, 14, '2020-02-08T10:00:00.000Z', '2020-02-12T10:00:00.000Z', 85, 340);
-- insert into Reservations (id, user_id, room_id, start_date, end_date, price, total) values (29, 21, 39, '2019-12-08T10:00:00.000Z', '2019-12-09T10:00:00.000Z', 150, 150);
-- insert into Reservations (id, user_id, room_id, start_date, end_date, price, total) values (30, 3, 38, '2020-03-20T10:00:00.000Z', '2020-03-23T10:00:00.000Z', 185, 555);
--
-- insert into Reviews (id, reservation_id, rating) values (1, 1, 4);
-- insert into Reviews (id, reservation_id, rating) values (2, 3, 5);
-- insert into Reviews (id, reservation_id, rating) values (3, 4, 5);
-- insert into Reviews (id, reservation_id, rating) values (4, 5, 5);
-- insert into Reviews (id, reservation_id, rating) values (5, 8, 4);
-- insert into Reviews (id, reservation_id, rating) values (6, 9, 3);
-- insert into Reviews (id, reservation_id, rating) values (7, 10, 2);
-- insert into Reviews (id, reservation_id, rating) values (8, 12, 2);
-- insert into Reviews (id, reservation_id, rating) values (9, 13, 5);
-- insert into Reviews (id, reservation_id, rating) values (10, 14, 4);
-- insert into Reviews (id, reservation_id, rating) values (11, 15, 3);
-- insert into Reviews (id, reservation_id, rating) values (12, 16, 2);
-- insert into Reviews (id, reservation_id, rating) values (13, 17, 5);
-- insert into Reviews (id, reservation_id, rating) values (14, 18, 4);
-- insert into Reviews (id, reservation_id, rating) values (15, 19, 4);
-- insert into Reviews (id, reservation_id, rating) values (16, 20, 4);
-- insert into Reviews (id, reservation_id, rating) values (17, 21, 5);
-- insert into Reviews (id, reservation_id, rating) values (18, 25, 5);
-- insert into Reviews (id, reservation_id, rating) values (19, 26, 4);
-- insert into Reviews (id, reservation_id, rating) values (20, 28, 1);
-- insert into Reviews (id, reservation_id, rating) values (21, 29, 2);
-- insert into Reviews (id, reservation_id, rating) values (22, 30, 3);

-- Необходимо вывести рейтинг для команат, которые хоть раз арендовали, как среднее значение рейтинга отзывов
-- округленное до целого вниз
SELECT room_id, FLOOR(AVG(rating)) as rating
FROM Reservations
JOIN Reviews ON Reviews.reservation_id = Reservations.id
GROUP BY room_id
ORDER BY rating DESC;

-- Вывести список комнат со всеми удобствами (наличие ТВ, интернета, кухни и кондиционера), а также общее количество
-- дней и сумму за все дни аренды каждой из таких комнат.
SELECT home_type, address, IFNULL(SUM(TIMESTAMPDIFF(DAY, start_date, end_date)), 0) AS days, IFNULL(SUM(total), 0) AS total_fee
FROM Rooms
LEFT JOIN Reservations on Reservations.room_id = Rooms.id
WHERE has_tv = 1 AND has_kitchen = 1 AND has_internet = 1 AND has_air_con = 1
GROUP BY Rooms.id;