insert into category (id, name , description) values (1, 'Electronic', 'Electronic');
insert into category (id, name , description) values (2, 'Computers', 'Computers');
insert into category (id, name , description) values (3, 'Smartphones', 'Smartphones');

insert into product (id, name, description, price, rating, image, stock) values (1, 'POCO F1', 'POCO F1', 15560, 5.0, 'image', 100);
insert into product (id, name, description, price, rating, image, stock) values (2, 'POCO F2', 'POCO F2', 16560, 5.0, 'image', 100);
insert into product (id, name, description, price, rating, image, stock) values (3, 'POCO F3', 'POCO F3', 17560, 5.0, 'image', 100);
insert into product (id, name, description, price, rating, image, stock) values (4, 'POCO F4', 'POCO F4', 18560, 5.0, 'image', 100);
insert into product (id, name, description, price, rating, image, stock) values (5, 'Redmi Note 7', 'Redmi Note 7', 19560, 5.0, 'image', 100);
insert into product (id, name, description, price, rating, image, stock) values (6, 'Redmi Note 8', 'Redmi Note 8', 25560, 5.0, 'image', 100);
insert into product (id, name, description, price, rating, image, stock) values (7, 'Redmi Note 9', 'Redmi Note 9', 26560, 5.0, 'image', 100);
insert into product (id, name, description, price, rating, image, stock) values (8, 'Redmi Note 10', 'Redmi Note 10', 27560, 5.0, 'image', 100);
insert into product (id, name, description, price, rating, image, stock) values (9, 'Apple iPhone 11', 'Apple iPhone 11', 55560, 5.0, 'image', 100);
insert into product (id, name, description, price, rating, image, stock) values (10, 'Apple iPhone 12', 'Apple iPhone 12', 75560, 5.0, 'image', 100);

insert into productcategory (id, product_id, category_id) values (1, 1, 1);
insert into productcategory (id, product_id, category_id) values (2, 1, 3);
insert into productcategory (id, product_id, category_id) values (3, 2, 1);
insert into productcategory (id, product_id, category_id) values (4, 2, 3);
insert into productcategory (id, product_id, category_id) values (5, 3, 1);
insert into productcategory (id, product_id, category_id) values (6, 3, 3);
insert into productcategory (id, product_id, category_id) values (7, 4, 1);
insert into productcategory (id, product_id, category_id) values (8, 4, 3);
insert into productcategory (id, product_id, category_id) values (9, 5, 1);
insert into productcategory (id, product_id, category_id) values (10, 5, 3);
insert into productcategory (id, product_id, category_id) values (11, 6, 1);
insert into productcategory (id, product_id, category_id) values (12, 6, 3);
insert into productcategory (id, product_id, category_id) values (13, 7, 1);
insert into productcategory (id, product_id, category_id) values (14, 7, 3);
insert into productcategory (id, product_id, category_id) values (15, 8, 1);
insert into productcategory (id, product_id, category_id) values (16, 8, 3);
insert into productcategory (id, product_id, category_id) values (17, 9, 1);
insert into productcategory (id, product_id, category_id) values (18, 9, 3);
insert into productcategory (id, product_id, category_id) values (19, 10, 1);
insert into productcategory (id, product_id, category_id) values (29, 10, 3);

insert into client (id, name, last_name, age, email) values (1, 'Dima', 'Bespalov', 40, 'bda82@mail.ru');
insert into client (id, name, last_name, age, email) values (2, 'Vova', 'Ivanov', 30, 'vova@example.ru');
insert into client (id, name, last_name, age, email) values (3, 'Ivan', 'Vladimirov', 20, 'ivan@example.ru');

insert into cart (id, client_id) values (1, 1);
insert into cart (id, client_id) values (2, 2);
insert into cart (id, client_id) values (3, 3);

insert into productcart (id, cart_id, product_id, stock) values (1, 1, 1, 1);
insert into productcart (id, cart_id, product_id, stock) values (2, 1, 2, 2);
insert into productcart (id, cart_id, product_id, stock) values (3, 1, 3, 3);
insert into productcart (id, cart_id, product_id, stock) values (4, 2, 4, 4);
insert into productcart (id, cart_id, product_id, stock) values (5, 3, 5, 5);
insert into productcart (id, cart_id, product_id, stock) values (6, 3, 6, 6);

insert into city (id, name, code) values (1, 'Taganrog', '+7');
insert into city (id, name, code) values (2, 'Rostov', '+7');
insert into city (id, name, code) values (3, 'London', '+10');

insert into country (id, name, code) values (1, 'Russia', 'RU');
insert into country (id, name, code) values (2, 'England', 'EN');

insert into address (id, city_id, country_id, line_1, line_2, loc) values (1, 1, 1, 'Frunze st', '63 k.39', 'POINT(47.23617 38.89688)');
insert into address (id, city_id, country_id, line_1, line_2, loc) values (2, 2, 1, 'Malinovskogo', '1 k. 2', 'POINT(47.233334 39.700001)');
insert into address (id, city_id, country_id, line_1, line_2, loc) values (3, 3, 2, 'Baker st', '256 k. 1', 'POINT(51.507359 -0.136439)');
insert into address (id, city_id, country_id, line_1, line_2, loc) values (4, 1, 1, 'Gogolevsky st', '11', 'POINT(47.23611 38.89681)');

insert into clientaddress (id, client_id, address_id, main) values (1, 1, 1, true);
insert into clientaddress (id, client_id, address_id, main) values (2, 2, 2, true);
insert into clientaddress (id, client_id, address_id, main) values (3, 3, 3, true);

insert into warehouse (id, name, address_id) values (1, 'Main WH', 4);
insert into warehouse (id, name, address_id) values (2, 'Second WH', 3);

insert into area (id, warehouse_id, points) values (1, 1, ST_GeogFromText('POLYGON((47.23610 38.89680, 47.23620 38.89680, 47.23620 38.89690, 47.23610 38.89690, 47.23610 38.89680))'));
insert into area (id, warehouse_id, points) values (2, 2, ST_GeogFromText('POLYGON((51.507350 -0.136430, 51.507360 -0.136430, 51.507360 -0.136440, 51.507350 -0.136440, 51.507350 -0.136430))'));
