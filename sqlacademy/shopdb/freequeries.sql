-- сумма корзины по каждому клиенту
select client.id, client.name, sum(productcart.stock * product.price)
from client
join cart on cart.id = client.id
join productcart on productcart.cart_id = client.id
join product on product.id = productcart.product_id
group by client.id;
-- 2	Vova	74240
-- 3	Ivan	251160
-- 1	Dima	101360

-- список товаров по клиенту с именем
select client.id, client.name, productcart.stock, product.name, product.price
from client
join cart on client.id = cart.client_id
join productcart on productcart.cart_id = cart.id
join product on product.id = productcart.product_id
where client.name = 'Ivan';
-- 3	Ivan	5	Redmi Note 7	19560
-- 3	Ivan	6	Redmi Note 8	25560

-- стоимость заказа по клиенту с именем
select client.id, client.name, sum(productcart.stock * product.price)
from client
join cart on client.id = cart.client_id
join productcart on productcart.cart_id = cart.id
join product on product.id = productcart.product_id
where client.name = 'Ivan'
group by client.id;

-- стоимость заказа по клиенту с именем и число товаров вообще
select client.id, client.name, sum(productcart.stock), sum(productcart.stock * product.price)
from client
join cart on client.id = cart.client_id
join productcart on productcart.cart_id = cart.id
join product on product.id = productcart.product_id
where client.name = 'Ivan'
group by client.id;

-- адреса доставки заказов клиента
select
	client.id,
	concat(client.name, ' ', client.last_name) as client,
	concat(sum(productcart.stock), ' items') as stock,
	concat(address.line_1, ' ', address.line_2) as address,
	concat(sum(productcart.stock * product.price), ' rub.') as total
from client
join clientaddress on clientaddress.client_id = client.id
join address on clientaddress.address_id = address.id
join cart on client.id = cart.client_id
join productcart on productcart.cart_id = cart.id
join product on product.id = productcart.product_id
group by client.id, address.line_1, address.line_2;