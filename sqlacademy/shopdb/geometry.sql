-- расстояние между клиентом и складом
select
	client.id,
	concat(client.name, ' ', client.last_name) as client,
	ST_DISTANCE(address.loc, wh.loc),
	wh.name
from client
join clientaddress on clientaddress.client_id = client.id
join address on clientaddress.address_id = address.id
cross join (
	select * from warehouse join address on warehouse.address_id = address.id
) wh;

select * from warehouse join address on warehouse.address_id = address.id;

select wh.id, wh.name, ct.name, cn.name, ad.loc
from warehouse as wh
join address as ad on wh.address_id = ad.id
join city as ct on ad.city_id = ct.id
join country as cn on ad.country_id = cn.id;

--  view wh_addresses
create view wh_addresses
as select wh.id as wid, wh.name as wname, ct.name as ctname, cn.name as cnname, ad.loc as loc
from warehouse as wh
join address as ad on wh.address_id = ad.id
join city as ct on ad.city_id = ct.id
join country as cn on ad.country_id = cn.id;

-- view wh with areas
create view wh_areas
as select wh.id as whid, wh.name as whname, addr.loc as whcenter, a.points as apoints
from warehouse as wh
join area as a on wh.id = a.warehouse_id
join address as addr on wh.address_id = addr.id;

-- view client addresses
create view client_location
as select client.id as client_id, A.id as addr_id, A.loc as addr_loc
from client
join ClientAddress ca on Client.id = ca.client_id
join Address A on ca.address_id = A.id;

-- view and request (расстояние от каждого клиента до каждого склада)
select
	client.id,
	concat(client.name, ' ', client.last_name) as client,
	ST_DISTANCE(address.loc, wha.loc),
	wha.wname
from client
join clientaddress on clientaddress.client_id = client.id
join address on clientaddress.address_id = address.id
cross join wh_addresses as wha;
-- 1,Dima Bespalov,9.35312268,Main WH
-- 1,Dima Bespalov,4344122.60939138,Second WH
-- 2,Vova Ivanov,89171.53845238,Main WH
-- 2,Vova Ivanov,4432698.45988645,Second WH
-- 3,Ivan Vladimirov,4344115.50897607,Main WH
-- 3,Ivan Vladimirov,0,Second WH


-- попадание точки в радиус 10 с центром
select wid, loc
from wh_addresses
where ST_DWithin(loc::geography, ST_GeogFromText('POINT(47.23615 38.89685)'), 10, false);

-- адрес доставки попадает в охват склада
select *
from wh_addresses as wha
cross join address as addr
where ST_DWithin(addr.loc::geography, wha.loc::geography, 5, false);


-- point radius
SELECT ST_Buffer(ST_GeomFromText('POINT(47.23615 38.89685)'), 50, 'quad_segs=8');


SELECT ST_AsText(points) FROM area;

SELECT ST_Distance(
  'SRID=4326;POINT(-118.4079 33.9434)'::geometry, -- Los Angeles (LAX)
  'SRID=4326;POINT(2.5559 49.0083)'::geometry     -- Paris (CDG)
  );

select
	ST_Contains(
	ST_GeomFromText('POINT ( -0.847 24.615 )'),
	ST_GeomFromText('POLYGON (( 26.314 -32.134, -26.556 -9.337, 24.373 53.233, 69.24 16.855, 26.314 -32.134 ))')
);

-- точка клиента в полигоне склада
select client_location.client_id, st_contains(wh_areas.apoints::geometry, client_location.addr_loc::geometry)
from client_location
cross join wh_areas;