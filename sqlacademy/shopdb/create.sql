create table Category (
	id int primary key,
	name varchar,
	description varchar
);

create table Product (
	id int primary key,
	name varchar,
	description varchar,
	price decimal,
	rating int,
	image varchar,
	stock int
);

create table ProductCategory (
	id int primary key,
	product_id int,
	category_id int,
	foreign key (product_id) references Product(id),
	foreign key (category_id) references Category(id)
);

create table Client (
	id int primary key,
	name varchar,
	last_name varchar,
	age int,
	email varchar
);

create table Cart (
	id int primary key,
	client_id int,
	foreign key (client_id) references Client(id)
);

create table ProductCart (
	id int primary key,
	cart_id int,
	product_id int,
	stock int,
	foreign key (product_id) references Product(id),
	foreign key (cart_id) references Cart(id)
);

create table City (
	id int primary key,
	name varchar,
	code varchar
);

create table Country (
	id int primary key,
	name varchar,
	code varchar
);

create table Address (
	id int primary key,
	city_id int,
	country_id int,
	line_1 varchar,
	line_2 varchar,
	loc geography(point),
	foreign key (city_id) references City(id),
	foreign key (country_id) references Country(id)
);

create table ClientAddress (
	id int primary key,
	client_id int,
	address_id int,
	main bool,
	foreign key (client_id) references Client(id),
	foreign key (address_id) references Address(id)
);

create table Warehouse (
	id int primary key,
	name varchar,
	address_id int,
	foreign key (address_id) references Address(id)
);

create table area (
	id int primary key,
	warehouse_id int,
	points geography(POLYGON),
	foreign key (warehouse_id) references Warehouse(id)
);

-- CREATE EXTENSION "postgis";