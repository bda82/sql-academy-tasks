------------------------------------------------------------------------------------------------------------------------
-- Family
------------------------------------------------------------------------------------------------------------------------
create table if not exists GoodTypes (
    good_type_id int primary key,
    good_type_name varchar(255)
);

create table if not exists FamilyMembers (
    member_id int primary key,
    status varchar(255),
    member_name varchar(255),
    birthday date
);

create table if not exists Goods (
    good_id int primary key,
    good_name varchar(255),
    type int,

    foreign key (type) references GoodTypes(good_type_id)
);

create table if not exists Payments (
    payment_id int primary key,
    family_member int,
    good int,
    amount int,
    unit_price int,
    date date,

    foreign key (good) references Goods(good_id),
    foreign key (family_member) references FamilyMembers(member_id)
);

-- insert into GoodTypes (good_type_id, good_type_name) values (1, 'communal payments');
-- insert into GoodTypes (good_type_id, good_type_name) values (2,	'food');
-- insert into GoodTypes (good_type_id, good_type_name) values (3,	'delicacies');
-- insert into GoodTypes (good_type_id, good_type_name) values (4,	'entertainment');
-- insert into GoodTypes (good_type_id, good_type_name) values (5,	'treatment');
-- insert into GoodTypes (good_type_id, good_type_name) values (6,	'education');
-- insert into GoodTypes (good_type_id, good_type_name) values (7,	'clothes');
-- insert into GoodTypes (good_type_id, good_type_name) values (8,	'equipment');
--
-- insert into FamilyMembers (member_id, status, member_name, birthday) values (1,	'father',	'Headley Quincey',	'1960-05-13T00:00:00.000Z');
-- insert into FamilyMembers (member_id, status, member_name, birthday) values (2,	'mother',	'Flavia Quincey',	'1963-02-16T00:00:00.000Z');
-- insert into FamilyMembers (member_id, status, member_name, birthday) values (3,	'son',	'Andie Quincey',	'1983-06-05T00:00:00.000Z');
-- insert into FamilyMembers (member_id, status, member_name, birthday) values (4,	'daughter',	'Lela Quincey',	'1985-06-07T00:00:00.000Z');
-- insert into FamilyMembers (member_id, status, member_name, birthday) values (5,	'daughter',	'Annie Quincey',	'1988-04-10T00:00:00.000Z');
-- insert into FamilyMembers (member_id, status, member_name, birthday) values (6,	'father',	'Ernest Forrest',	'1961-09-11T00:00:00.000Z');
-- insert into FamilyMembers (member_id, status, member_name, birthday) values (7,	'mother',	'Constance Forrest',	'1968-09-06T00:00:00.000Z');
--
-- insert into Goods (good_id, good_name, type) values (1,	'apartment fee', 1);
-- insert into Goods (good_id, good_name, type) values (2,	'phone fee', 1);
-- insert into Goods (good_id, good_name, type) values (3,	'bread',	2);
-- insert into Goods (good_id, good_name, type) values (4,	'milk',	2);
-- insert into Goods (good_id, good_name, type) values (5,	'red caviar', 3);
-- insert into Goods (good_id, good_name, type) values (6,	'cinema', 4);
-- insert into Goods (good_id, good_name, type) values (7,	'black caviar',	3);
-- insert into Goods (good_id, good_name, type) values (8,	'cough tablets', 5);
-- insert into Goods (good_id, good_name, type) values (9,	'potato', 2);
-- insert into Goods (good_id, good_name, type) values (10, 'pineapples', 3);
-- insert into Goods (good_id, good_name, type) values (11, 'television', 8);
-- insert into Goods (good_id, good_name, type) values (12, 'vacuum cleaner', 8);
-- insert into Goods (good_id, good_name, type) values (13, 'jacket', 7);
-- insert into Goods (good_id, good_name, type) values (14, 'fur coat', 7);
-- insert into Goods (good_id, good_name, type) values (15, 'music school fee', 6);
-- insert into Goods (good_id, good_name, type) values (16, 'english school fee', 6);
--
-- insert into Payments (payment_id, family_member, good, amount, unit_price, date) values (1,	1, 1, 1, 2000, '2005-02-12T00:00:00.000Z');
-- insert into Payments (payment_id, family_member, good, amount, unit_price, date) values (2,	2, 1, 1, 2100, '2005-03-23T00:00:00.000Z');
-- insert into Payments (payment_id, family_member, good, amount, unit_price, date) values (3,	3, 4, 5, 20, '2005-05-14T00:00:00.000Z');
-- insert into Payments (payment_id, family_member, good, amount, unit_price, date) values (4,	4, 5, 1, 350, '2005-07-22T00:00:00.000Z');
-- insert into Payments (payment_id, family_member, good, amount, unit_price, date) values (5,	4, 7, 2, 150, '2005-07-26T00:00:00.000Z');
-- insert into Payments (payment_id, family_member, good, amount, unit_price, date) values (6,	5, 6, 1, 100, '2005-02-20T00:00:00.000Z');
-- insert into Payments (payment_id, family_member, good, amount, unit_price, date) values (7,	2, 6, 1, 120, '2005-07-30T00:00:00.000Z');
-- insert into Payments (payment_id, family_member, good, amount, unit_price, date) values (8,	2, 16, 1, 5500, '2005-09-12T00:00:00.000Z');
-- insert into Payments (payment_id, family_member, good, amount, unit_price, date) values (9,	5, 15, 1, 230, '2005-09-30T00:00:00.000Z');
-- insert into Payments (payment_id, family_member, good, amount, unit_price, date) values (10, 5, 15, 1, 230, '2005-10-27T00:00:00.000Z');
-- insert into Payments (payment_id, family_member, good, amount, unit_price, date) values (11, 5, 15, 1, 250, '2005-11-28T00:00:00.000Z');
-- insert into Payments (payment_id, family_member, good, amount, unit_price, date) values (12, 5, 15, 1, 250, '2005-12-22T00:00:00.000Z');
-- insert into Payments (payment_id, family_member, good, amount, unit_price, date) values (13, 3, 13, 1, 2200, '2005-08-11T00:00:00.000Z');
-- insert into Payments (payment_id, family_member, good, amount, unit_price, date) values (14, 2, 14, 1, 66000, '2005-10-23T00:00:00.000Z');
-- insert into Payments (payment_id, family_member, good, amount, unit_price, date) values (15, 1, 9, 5, 8, '2005-02-03T00:00:00.000Z');
-- insert into Payments (payment_id, family_member, good, amount, unit_price, date) values (16, 1, 9, 5, 7, '2005-03-11T00:00:00.000Z');
-- insert into Payments (payment_id, family_member, good, amount, unit_price, date) values (17, 2, 9, 3, 8, '2005-03-18T00:00:00.000Z');
-- insert into Payments (payment_id, family_member, good, amount, unit_price, date) values (18, 1, 9, 8, 8, '2005-04-20T00:00:00.000Z');
-- insert into Payments (payment_id, family_member, good, amount, unit_price, date) values (19, 1, 9, 5, 7, '2005-05-13T00:00:00.000Z');
-- insert into Payments (payment_id, family_member, good, amount, unit_price, date) values (20, 2, 9, 3, 150, '2005-06-11T00:00:00.000Z');
-- insert into Payments (payment_id, family_member, good, amount, unit_price, date) values (21, 3, 10, 1, 100, '2006-01-12T00:00:00.000Z');
-- insert into Payments (payment_id, family_member, good, amount, unit_price, date) values (22, 1, 5, 3, 10, '2006-03-12T00:00:00.000Z');
-- insert into Payments (payment_id, family_member, good, amount, unit_price, date) values (23, 1, 8, 1, 300, '2005-06-05T00:00:00.000Z');
-- insert into Payments (payment_id, family_member, good, amount, unit_price, date) values (24, 3, 6, 8, 150, '2005-06-20T00:00:00.000Z');

-- Определить, сколько потратил в 2005 году каждый из членов семьи
SELECT member_name, status, SUM(amount * unit_price) AS costs
FROM FamilyMembers
JOIN Payments ON Payments.family_member = FamilyMembers.member_id
WHERE YEAR(date) = 2005
GROUP BY member_name, status;

-- Узнать, кто старше всех в семьe
SELECT member_name
FROM FamilyMembers
WHERE birthday = (SELECT MIN(birthday) FROM FamilyMembers);

-- Определить, кто из членов семьи покупал картошку (potato)
SELECT DISTINCT status
FROM FamilyMembers
JOIN Payments ON Payments.family_member = FamilyMembers.member_id
JOIN Goods ON Goods.good_id = Payments.good
WHERE good_name = 'potato';

-- Сколько и кто из семьи потратил на развлечения (entertainment). Вывести статус в семье, имя, сумму
SELECT status, member_name, SUM(amount * unit_price) as costs
FROM FamilyMembers
JOIN Payments ON FamilyMembers.member_id = Payments.family_member
JOIN Goods ON Payments.good = Goods.good_id
JOIN GoodTypes ON Goods.type = GoodTypes.good_type_id
WHERE good_type_name = 'entertainment'
GROUP BY status, member_name;

-- Определить товары, которые покупали более 1 раза
SELECT good_name
FROM Goods
JOIN Payments ON Goods.good_id = Payments.good
GROUP BY good
HAVING count(Payments.payment_id) > 1;

-- Найти имена всех матерей (mother)
SELECT DISTINCT member_name
FROM FamilyMembers
WHERE status = 'mother';

-- Найдите самый дорогой деликатес (delicacies) и выведите его стоимость
SELECT good_name, unit_price
FROM Goods
JOIN Payments ON Payments.good = Goods.good_id
WHERE unit_price = (
    SELECT MAX(unit_price) as price
    FROM Payments
    JOIN Goods ON Goods.good_id = Payments.good
    JOIN GoodTypes ON GoodTypes.good_type_id = Goods.type
    WHERE good_type_name = 'delicacies'
);

-- Определить кто и сколько потратил в июне 2005
SELECT member_name, SUM(amount * unit_price ) as costs
FROM FamilyMembers
JOIN Payments ON FamilyMembers.member_id = Payments.family_member
WHERE EXTRACT(month from date) = 06 AND EXTRACT(year from date) = 2005
GROUP BY member_name;

-- Определить, какие товары не покупались в 2005 году
SELECT good_name
FROM Goods
WHERE good_id NOT IN (
    SELECT good
    FROM Payments
    WHERE EXTRACT(year from date) = 2005
);

-- Определить группы товаров, которые не приобретались в 2005 году
SELECT good_type_name
FROM GoodTypes
WHERE good_type_id NOT IN (
    SELECT good_type_id FROM GoodTypes
    JOIN Goods ON Goods.type = good_type_id
    JOIN Payments ON Payments.good = Goods.good_id
    WHERE EXTRACT(year from date) = 2005
);

-- Узнать, сколько потрачено на каждую из групп товаров в 2005 году. Вывести название группы и сумму
SELECT good_type_name, SUM(amount * unit_price) AS costs
FROM GoodTypes
JOIN Goods ON Goods.type = GoodTypes.good_type_id
JOIN Payments ON Payments.good = Goods.good_id
WHERE EXTRACT(year from date) = 2005
GROUP BY good_type_name;

-- Вывести всех членов семьи с фамилией Quincey.
SELECT *
FROM FamilyMembers
WHERE member_name LIKE '% Quincey';

-- Вывести средний возраст людей (в годах), хранящихся в базе данных. Результат округлите до целого в меньшую сторону.
SELECT
FLOOR(
    AVG(
        EXTRACT(year from NOW()) - EXTRACT(year from birthday)
    )
) as age
FROM FamilyMembers;

-- Найдите среднюю стоимость икры. В базе данных хранятся данные о покупках красной (red caviar) и черной икры (black caviar).
SELECT AVG(unit_price)
AS cost
FROM Payments
WHERE good IN (
    SELECT good_id
    FROM Goods
    WHERE good_name = 'red caviar' OR good_name = 'black caviar'
);

-- Добавьте товар с именем "Cheese" и типом "food" в список товаров (Goods).
INSERT INTO Goods
SET good_name = 'Cheese',
    type = (SELECT good_type_id FROM GoodTypes WHERE good_type_name = 'food'),
    good_id = (SELECT COUNT(*) + 1 FROM Goods AS a);

-- Добавьте в список типов товаров (GoodTypes) новый тип "auto".
INSERT INTO GoodTypes
SET good_type_name = 'auto',
    good_type_id = (SELECT COUNT(*) + 1 FROM GoodTypes AS a);

-- Измените имя "Andie Quincey" на новое "Andie Anthony".
UPDATE FamilyMembers
SET member_name = 'Andie Anthony'
WHERE member_name = 'Andie Quincey';

-- Удалить всех членов семьи с фамилией "Quincey".
DELETE FROM FamilyMembers
WHERE member_name LIKE '%Quincey';