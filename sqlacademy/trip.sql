------------------------------------------------------------------------------------------------------------------------
-- Trip
------------------------------------------------------------------------------------------------------------------------
create table if not exists Company (
    id int primary key,
    name varchar(255)
);

create table if not exists Trip (
    id int primary key ,
    company int,
    plane varchar(255),
    town_from varchar(255),
    town_to varchar(255),
    time_out date,
    time_in date,

    foreign key (company) references Company(id)
);

create table if not exists Passenger (
    id int primary key,
    name varchar(255)
);

create table if not exists PassInTrip (
    id int primary key,
    trip int,
    passenger int,
    place varchar(255),

    foreign key (trip) references Trip(id),
    foreign key (passenger) references Passenger(id)
);

-- insert into Company (id, name) values (1, 'Don Avia');
-- insert into Company (id, name) values (2, 'Aeroflot');
-- insert into Company (id, name) values (3, 'Dale Avia');
-- insert into Company (id, name) values (4, 'Air France');
-- insert into Company (id, name) values (5, 'British AW');
--
-- insert into Passenger (id, name) values (1,	'Bruce Willis');
-- insert into Passenger (id, name) values (2,	'George Clooney');
-- insert into Passenger (id, name) values (3,	'Kevin Costner');
-- insert into Passenger (id, name) values (4,	'Donald Sutherland');
-- insert into Passenger (id, name) values (5,	'Jennifer Lopez');
-- insert into Passenger (id, name) values (6,	'Ray Liotta');
-- insert into Passenger (id, name) values (7,	'Samuel L. Jackson');
-- insert into Passenger (id, name) values (8,	'Nikole Kidman');
-- insert into Passenger (id, name) values (9,	'Alan Rickman');
-- insert into Passenger (id, name) values (10,	'Kurt Russell');
-- insert into Passenger (id, name) values (11,	'Harrison Ford');
-- insert into Passenger (id, name) values (12,	'Russell Crowe');
-- insert into Passenger (id, name) values (13,	'Steve Martin');
-- insert into Passenger (id, name) values (14,	'Michael Caine');
-- insert into Passenger (id, name) values (15,	'Angelina Jolie');
-- insert into Passenger (id, name) values (16,	'Mel Gibson');
-- insert into Passenger (id, name) values (17,	'Michael Douglas');
-- insert into Passenger (id, name) values (18,	'John Travolta');
-- insert into Passenger (id, name) values (19,	'Sylvester Stallone');
-- insert into Passenger (id, name) values (20,	'Tommy Lee Jones');
-- insert into Passenger (id, name) values (21,	'Catherine Zeta-Jones');
-- insert into Passenger (id, name) values (22,	'Antonio Banderas');
-- insert into Passenger (id, name) values (23,	'Kim Basinger');
-- insert into Passenger (id, name) values (24,	'Sam Neill');
-- insert into Passenger (id, name) values (25,	'Gary Oldman');
-- insert into Passenger (id, name) values (26,	'ClINT Eastwood');
-- insert into Passenger (id, name) values (27,	'Brad Pitt');
-- insert into Passenger (id, name) values (28,	'Johnny Depp');
-- insert into Passenger (id, name) values (29,	'Pierce Brosnan');
-- insert into Passenger (id, name) values (30,	'Sean Connery');
-- insert into Passenger (id, name) values (31,	'Bruce Willis');
-- insert into Passenger (id, name) values (37,	'Mullah Omar');
--
-- insert into Trip (id, company, plane, town_from, town_to, time_out, time_in) values (1100, 4, 'Boeing', 'Rostov',	'Paris',	'1900-01-01T14:30:00.000Z', '1900-01-01T17:50:00.000Z');
-- insert into Trip (id, company, plane, town_from, town_to, time_out, time_in) values (1101, 4, 'Boeing', 'Paris',	'Rostov',	'1900-01-01T08:12:00.000Z', '1900-01-01T11:45:00.000Z');
-- insert into Trip (id, company, plane, town_from, town_to, time_out, time_in) values (1123, 3, 'TU-154', 'Rostov',	'Vladivostok',	'1900-01-01T16:20:00.000Z', '1900-01-02T03:40:00.000Z');
-- insert into Trip (id, company, plane, town_from, town_to, time_out, time_in) values (1124, 3, 'TU-154', 'Vladivostok',	'Rostov',	'1900-01-01T09:00:00.000Z', '1900-01-01T19:50:00.000Z');
-- insert into Trip (id, company, plane, town_from, town_to, time_out, time_in) values (1145, 2, 'IL-86', 'Moscow',	'Rostov',	'1900-01-01T09:35:00.000Z', '1900-01-01T11:23:00.000Z');
-- insert into Trip (id, company, plane, town_from, town_to, time_out, time_in) values (1146, 2, 'IL-86', 'Rostov',	'Moscow',	'1900-01-01T17:55:00.000Z', '1900-01-01T20:01:00.000Z');
-- insert into Trip (id, company, plane, town_from, town_to, time_out, time_in) values (1181, 1, 'TU-134', 'Rostov',	'Moscow',	'1900-01-01T06:12:00.000Z', '1900-01-01T08:01:00.000Z');
-- insert into Trip (id, company, plane, town_from, town_to, time_out, time_in) values (1182, 1, 'TU-134', 'Moscow',	'Rostov',	'1900-01-01T12:35:00.000Z', '1900-01-01T14:30:00.000Z');
-- insert into Trip (id, company, plane, town_from, town_to, time_out, time_in) values (1187, 1, 'TU-134', 'Rostov',	'Moscow',	'1900-01-01T15:42:00.000Z', '1900-01-01T17:39:00.000Z');
-- insert into Trip (id, company, plane, town_from, town_to, time_out, time_in) values (1188, 1, 'TU-134', 'Moscow',	'Rostov',	'1900-01-01T22:50:00.000Z', '1900-01-02T00:48:00.000Z');
-- insert into Trip (id, company, plane, town_from, town_to, time_out, time_in) values (1195, 1, 'TU-154', 'Rostov',	'Moscow',	'1900-01-01T23:30:00.000Z', '1900-01-02T01:11:00.000Z');
-- insert into Trip (id, company, plane, town_from, town_to, time_out, time_in) values (1196, 1, 'TU-154', 'Moscow',	'Rostov',	'1900-01-01T04:00:00.000Z', '1900-01-01T05:45:00.000Z');
-- insert into Trip (id, company, plane, town_from, town_to, time_out, time_in) values (7771, 5, 'Boeing', 'London',	'Singapore',	'1900-01-01T01:00:00.000Z', '1900-01-01T11:00:00.000Z');
-- insert into Trip (id, company, plane, town_from, town_to, time_out, time_in) values (7772, 5, 'Boeing', 'Singapore',	'London',	'1900-01-01T12:00:00.000Z', '1900-01-02T02:00:00.000Z');
-- insert into Trip (id, company, plane, town_from, town_to, time_out, time_in) values (7773, 5, 'Boeing', 'London',	'Singapore',	'1900-01-01T03:00:00.000Z', '1900-01-01T13:00:00.000Z');
-- insert into Trip (id, company, plane, town_from, town_to, time_out, time_in) values (7774, 5, 'Boeing', 'Singapore',	'London',	'1900-01-01T14:00:00.000Z', '1900-01-02T06:00:00.000Z');
-- insert into Trip (id, company, plane, town_from, town_to, time_out, time_in) values (7775, 5, 'Boeing', 'London',	'Singapore',	'1900-01-01T09:00:00.000Z', '1900-01-01T20:00:00.000Z');
-- insert into Trip (id, company, plane, town_from, town_to, time_out, time_in) values (7776, 5, 'Boeing', 'Singapore',	'London',	'1900-01-01T18:00:00.000Z', '1900-01-02T08:00:00.000Z');
-- insert into Trip (id, company, plane, town_from, town_to, time_out, time_in) values (7777, 5, 'Boeing', 'London',	'Singapore',	'1900-01-01T18:00:00.000Z', '1900-01-02T06:00:00.000Z');
-- insert into Trip (id, company, plane, town_from, town_to, time_out, time_in) values (7778, 5, 'Boeing', 'Singapore',	'London',	'1900-01-01T22:00:00.000Z', '1900-01-02T12:00:00.000Z');
-- insert into Trip (id, company, plane, town_from, town_to, time_out, time_in) values (8881, 5, 'Boeing', 'London',	'Paris',	'1900-01-01T03:00:00.000Z', '1900-01-01T04:00:00.000Z');
-- insert into Trip (id, company, plane, town_from, town_to, time_out, time_in) values (8882, 5, 'Boeing', 'Paris',	'London',	'1900-01-01T22:00:00.000Z', '1900-01-01T23:00:00.000Z');
--
-- insert into PassInTrip (id, trip, passenger, place) values (1,	1100, 1, '1a');
-- insert into PassInTrip (id, trip, passenger, place) values (2,	1123, 3, '2a');
-- insert into PassInTrip (id, trip, passenger, place) values (3,	1123, 1, '4c');
-- insert into PassInTrip (id, trip, passenger, place) values (4,	1123, 6, '4b');
-- insert into PassInTrip (id, trip, passenger, place) values (5,	1124, 2, '2d');
-- insert into PassInTrip (id, trip, passenger, place) values (6,	1145, 3, '2c');
-- insert into PassInTrip (id, trip, passenger, place) values (7,	1181, 1, '1a');
-- insert into PassInTrip (id, trip, passenger, place) values (8,	1181, 6, '1b');
-- insert into PassInTrip (id, trip, passenger, place) values (9,	1181, 8, '3c');
-- insert into PassInTrip (id, trip, passenger, place) values (10,	1181, 5, '1b');
-- insert into PassInTrip (id, trip, passenger, place) values (11,	1182, 5, '4b');
-- insert into PassInTrip (id, trip, passenger, place) values (12,	1187, 8, '3a');
-- insert into PassInTrip (id, trip, passenger, place) values (13,	1188, 8, '3a');
-- insert into PassInTrip (id, trip, passenger, place) values (14,	1182, 9, '6d');
-- insert into PassInTrip (id, trip, passenger, place) values (15,	1145, 5, '1d');
-- insert into PassInTrip (id, trip, passenger, place) values (16,	1187, 10, '3d');
-- insert into PassInTrip (id, trip, passenger, place) values (17,	8882, 37, '1a');
-- insert into PassInTrip (id, trip, passenger, place) values (18,	7771, 37, '1c');
-- insert into PassInTrip (id, trip, passenger, place) values (19,	7772, 37, '1a');
-- insert into PassInTrip (id, trip, passenger, place) values (20,	8881, 37, '1d');
-- insert into PassInTrip (id, trip, passenger, place) values (21,	7778, 10, '2a');
-- insert into PassInTrip (id, trip, passenger, place) values (22,	7772, 10, '3a');
-- insert into PassInTrip (id, trip, passenger, place) values (23,	7771, 11, '4a');
-- insert into PassInTrip (id, trip, passenger, place) values (24,	7771, 11, '1b');
-- insert into PassInTrip (id, trip, passenger, place) values (25,	7771, 11, '5a');
-- insert into PassInTrip (id, trip, passenger, place) values (26,	7772, 12, '1d');
-- insert into PassInTrip (id, trip, passenger, place) values (27,	7773, 13, '2d');
-- insert into PassInTrip (id, trip, passenger, place) values (28,	7772, 13, '1b');
-- insert into PassInTrip (id, trip, passenger, place) values (29,	8882, 14, '3d');
-- insert into PassInTrip (id, trip, passenger, place) values (30,	7771, 14, '4d');
-- insert into PassInTrip (id, trip, passenger, place) values (31,	7771, 14, '5d');
-- insert into PassInTrip (id, trip, passenger, place) values (32,	7772, 14, '1c');

-- Вывести названия всеx авиакомпаний
SELECT name from Company;

-- Вывести имена людей, которые заканчиваются на "man"
SELECT name from Passenger WHERE name LIKE '%man';

-- Вывести количество рейсов, совершенных на TU-134
SELECT count(plane) as count FROM Trip WHERE plane = 'TU-134';

-- Какие компании совершали перелеты на Boeing
SELECT name FROM Company
JOIN Trip ON Trip.company = Company.id WHERE Trip.plane = 'Boeing' GROUP BY name;

-- Вывести все названия самолётов, на которых можно улететь в Москву (Moscow)
SELECT plane FROM Trip WHERE town_to = 'Moscow' GROUP BY plane;

-- Вывести все рейсы, совершенные из Москвы
SELECT * FROM Trip WHERE town_from  = 'Moscow';

-- Вывести имена всех когда-либо обслуживаемых пассажиров авиакомпаний
SELECT name FROM Passenger;

-- Какие компании организуют перелеты с Владивостока (Vladivostok)?
SELECT name from Company
JOIN Trip ON Trip.company = Company.id
WHERE town_from = 'Vladivostok';

-- Вывести вылеты, совершенные с 10 ч. по 14 ч. 1 января 1900 г.
SELECT * FROM Trip WHERE time_out BETWEEN '1900-01-01T10:00:00.000Z' AND '1900-01-01T14:00:00.000Z';

-- Вывести пассажиров с самым длинным именем
SELECT name
FROM Passenger
WHERE LENGTH(name) = (SELECT MAX(LENGTH(name)) FROM Passenger);

-- Вывести id и количество пассажиров для всех прошедших полётов
SELECT trip, COUNT(passenger) as count FROM PassInTrip GROUP BY trip;

-- Вывести имена людей, у которых есть полный тёзка среди пассажиров
SELECT name FROM Passenger GROUP BY name HAVING count(name) > 1;;

-- В какие города летал Bruce Willis
SELECT DISTINCT town_to
FROM Trip
JOIN PassInTrip ON PassInTrip.trip = Trip.id
JOIN Passenger ON PassInTrip.passenger = Passenger.id
WHERE Passenger.name = 'Bruce Willis';

-- Во сколько Стив Мартин (Steve Martin) прилетел в Лондон (London)
SELECT time_in
FROM Trip
JOIN PassInTrip ON Trip.id = PassInTrip.trip
JOIN Passenger ON PassInTrip.passenger = Passenger.id
WHERE Passenger.name = 'Steve Martin' AND Trip.town_to = 'London';

-- Вывести отсортированный по количеству перелетов (по убыванию) и имени (по возрастанию) список пассажиров, совершивших
-- хотя бы 1 полет.
SELECT name, count(*) as count
FROM Passenger
JOIN PassInTrip ON Passenger.id = PassInTrip.passenger
GROUP BY passenger
HAVING COUNT(trip) > 0
ORDER BY COUNT(trip) DESC, name;

-- Сколько рейсов совершили авиакомпании с Ростова (Rostov) в Москву (Moscow) ?
SELECT count(*) as count
FROM Trip
WHERE town_from = 'Rostov' AND town_to = 'Moscow';

-- Выведите имена пассажиров улетевших в Москву (Moscow) на самолете TU-134
SELECT DISTINCT name
FROM Passenger
JOIN PassInTrip ON PassInTrip.passenger = Passenger.id
JOIN Trip ON Trip.id = PassInTrip.trip
WHERE town_to = 'Moscow' and plane = 'TU-134';

-- Выведите нагруженность (число пассажиров) каждого рейса (trip). Результат вывести в отсортированном виде по убыванию нагруженности.
SELECT trip, COUNT(passenger) as count
FROM PassInTrip
GROUP BY trip
ORDER BY count DESC;

-- В какие города можно улететь из Парижа (Paris) и сколько времени это займёт?
SELECT town_to, TIMEDIFF(time_in, time_out) as flight_time
FROM Trip
WHERE town_from = 'Paris';

-- Удалить компании, совершившие наименьшее количество рейсов.
DELETE FROM Company
WHERE Company.id IN (
    SELECT company
    FROM Trip
    GROUP BY company
    HAVING count(id) = (
        SELECT MIN(count)
        FROM (
            SELECT count(id) as count
            FROM Trip
            GROUP BY company
        ) AS min_count
    )
);

-- Удалить все перелеты, совершенные из Москвы (Moscow).
DELETE FROM Trip
WHERE town_from = 'Moscow';

-- Удалить компании, совершившие наименьшее количество рейсов.
DELETE FROM Company
WHERE Company.id IN (
    SELECT company
    FROM Trip
    GROUP BY company
    HAVING COUNT(id) = (
        SELECT MIN(count)
        FROM (
            SELECT COUNT(id) AS count
            FROM Trip
            GROUP BY company
        ) AS min_count
    )
);
